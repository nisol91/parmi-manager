import React, { Component } from "react";
import { translate } from "react-i18next";
import "./table.scss";
import { Link } from "react-router-dom";
import { Spinner } from "reactstrap";
import { db } from "../portfolio_single_page/portfolio_sp";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowRight,
  faArrowLeft,
  faArrowUp,
  faArrowDown,
  faArrowAltCircleLeft,
  faWindowClose,
  faPlus
} from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import { BrowserRouter as Router, Route } from "react-router-dom";
import TopSearch from "../topSearch/topSearch";
import {
  useTable,
  useGroupBy,
  useFilters,
  useSortBy,
  useExpanded,
  usePagination
} from "react-table";
import { test } from "../shared_methods";
import Highlighter from "react-highlight-words";
import _ from "lodash";
import Toggle from "react-toggle";

class Table extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: this.props.location.state.title,
      key: "",
      fields: [],
      dataLoaded: false,
      fetchedData: [],
      loadSearchBar: false,
      token: "",
      deletePopup: false,
      idElementToDelete: null,
      searchVal: "",
      matchResultSearch: [],
      highlighEnabled: false,
      filterCounter: 0,
      showFilterArrow: false,
      pagesNumber: 1,
      pagesNumberSingleField: 1,
      fetchSingleFieldData: [],
      renderToggle: false
    };

    this.hideDeletePopup = this.hideDeletePopup.bind(this);
    this.handleChangeSearch = this.handleChangeSearch.bind(this);
    this.handleSubmitSearch = this.handleSubmitSearch.bind(this);
    this.emptySearch = this.emptySearch.bind(this);
  }

  // login check
  async login() {
    await axios
      .post("http://api.parmi.io/login_check", {
        username: "nicolasolzi91@gmail.com",
        password: "parmi1357"
      })
      .then(({ data }) => {
        console.log("userSignIn: ", data);
        if (data.payload.token) {
          localStorage.setItem("token", JSON.stringify(data.payload.token));
          localStorage.setItem("user", JSON.stringify(data.userdata));
          this.setState({
            token: data.payload.token
          });
        }
      })
      .catch(function(error) {
        console.log("Error****:", error.message);
      });
  }

  //======================init fetch data process

  // fetch data from api: the key for this table is set from the properties
  async fetchData(key) {
    // this.findNumberOfPages(key).then(() =>
    //   this.fetchDataCycle(key).then(
    //     async () =>
    //       await this.reorderFetchedData().then(
    //         async val => await this.setFetchedDataState(val)
    //       )
    //   )
    // );
    await this.findNumberOfPages(key);
    await this.fetchDataCycle(key);
    await this.reorderFetchedData().then(
      async () => await this.refactorUserAnswerTabFields()
    );
  }

  // find number of pages
  async findNumberOfPages(key) {
    var authToken = this.state.token;
    var fetchCall = {
      method: "GET",
      url: `http://api.parmi.io/api/${key}`,
      headers: {
        Authorization: "Bearer " + authToken,
        accept: "application/ld+json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      json: true
    };
    await axios(fetchCall)
      .then(response => {
        // handle success
        console.log("IL RESPONSO 1");
        console.log(response);
        console.log(response.data);
        console.log("hydra:view-----");
        console.log(response.data["hydra:view"]["hydra:last"]);
        var string = response.data["hydra:view"]["hydra:last"];
        if (string != null) {
          // string.substring

          var str = string;
          var matches = str.match(/(\d+)/);

          if (matches) {
            console.log("matches");
            console.log(matches[0]);
          }

          this.setState({
            pagesNumber: matches[0]
          });
          setTimeout(() => {
            console.log("this.state.pagesNumber");
            console.log(this.state.pagesNumber);
          }, 500);
        } else {
          this.setState({
            pagesNumber: 1
          });
          setTimeout(() => {
            console.log("this.state.pagesNumber");
            console.log(this.state.pagesNumber);
          }, 500);
        }
      })
      .catch(function(error) {
        // handle error
        console.log(error);
      })
      .finally(function() {
        // always executed
      });
  }

  // cycle through the pages (api platform has a fucking pagination)
  async fetchDataCycle(key) {
    for (let i = 1; i <= this.state.pagesNumber; i++) {
      var authToken = this.state.token;
      var fetchCall = {
        method: "GET",
        url: `http://api.parmi.io/api/${key}?page=${i}`,
        headers: {
          Authorization: "Bearer " + authToken,
          accept: "application/ld+json",
          "Content-Type": "application/x-www-form-urlencoded"
        },
        json: true
      };
      await axios(fetchCall)
        .then(response => {
          // handle success
          console.log("IL RESPONSO 2");

          console.log(response);
          console.log(response.data);
          console.log("hydra:member-----");
          console.log(response.data["hydra:member"]);

          var fetchedData = this.state.fetchedData;
          if (response.data["hydra:member"].length !== 0) {
            fetchedData.push(response.data["hydra:member"]);
          }
          this.setState({
            fetchedData: fetchedData
          });
          setTimeout(() => {
            // this.setFields();
            // console.log('this.props.location.state.key');
            // console.log(this.props.location.state.key);
            console.log("this.state.fetchedData--**");
            console.log(this.state.fetchedData);
          }, 100);

          console.log(" arrayyyyy");
          console.log(fetchedData);
        })
        .catch(function(error) {
          // handle error
          console.log(error);
        })
        .finally(function() {
          // always executed
        });
    }
  }

  async reorderFetchedData() {
    var fetchedData = this.state.fetchedData;
    var newFetchedData = [];
    for (let i = 0; i < fetchedData.length; i++) {
      console.log("fetchedData[i]");
      console.log(fetchedData[i]);
      for (let j = 0; j < fetchedData[i].length; j++) {
        console.log("fetchedData[i][j]");
        console.log(fetchedData[i][j]["@id"]);

        if (fetchedData[i] !== []) {
          newFetchedData.push(fetchedData[i][j]);
          console.log("newFetchedDataInsideFor");
          console.log(newFetchedData);
        }
      }
    }

    console.log("=================newFetchedData===================");
    console.log(newFetchedData);
    console.log("====================================");
    this.setState({
      dataLoaded: true,
      loadSearchBar: true,
      fetchedData: newFetchedData
    });
    return;
    // setTimeout(() => {
    //   console.log("this.state.fetchedDataForEdit--**");
    //   console.log(this.state.fetchedDataForEdit);
    // }, 500);
  }

  //======================finish fetch data process

  //****************************************************************** */
  //======================init GENERAL fetch data process for given key
  // (key is the api endpoint, for example 'users', 'cta_lists'..ecc)

  // fetch data from api
  async fetchSingleFieldData(key) {
    await this.findNumberOfPagesSingleField(key).then(() =>
      this.fetchDataCycleSingleField(key)
    );

    // for now it not need a reorder function such as in the table
    //   .then(() =>
    //   this.reorderFetchedData(key)
    // );
  }

  // find number of pages
  async findNumberOfPagesSingleField(key) {
    var authToken = this.state.token;
    var fetchCall = {
      method: "GET",
      url: `http://api.parmi.io/api/${key}`,
      headers: {
        Authorization: "Bearer " + authToken,
        accept: "application/ld+json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      json: true
    };
    await axios(fetchCall)
      .then(response => {
        // handle success
        console.log("IL RESPONSO 1");
        console.log(response);
        console.log(response.data);
        console.log("hydra:view-----");
        console.log(response.data["hydra:view"]["hydra:last"]);
        var string = response.data["hydra:view"]["hydra:last"];
        if (string != null) {
          // string.substring

          var str = string;
          var matches = str.match(/(\d+)/);

          if (matches) {
            console.log("matches");
            console.log(matches[0]);
          }

          this.setState(prevState => ({
            pagesNumberSingleField: {
              // object that we want to update
              ...prevState.pagesNumberSingleField, // keep all other key-value pairs
              [key]: matches[0] // update the value of specific key
            }
          }));
        } else {
          this.setState(prevState => ({
            pagesNumberSingleField: {
              // object that we want to update
              ...prevState.pagesNumberSingleField, // keep all other key-value pairs
              [key]: 1 // update the value of specific key
            }
          }));
        }
      })
      .catch(function(error) {
        // handle error
        console.log(error);
      })
      .finally(function() {
        // always executed
      });
  }

  // cycle through the pages (api platform has a fucking pagination)
  async fetchDataCycleSingleField(key) {
    for (let i = 1; i <= this.state.pagesNumberSingleField; i++) {
      var authToken = this.state.token;

      var fetchCall = {
        method: "GET",
        url: `http://api.parmi.io/api/${key}?page=${i}`,
        headers: {
          Authorization: "Bearer " + authToken,
          accept: "application/ld+json",
          "Content-Type": "application/x-www-form-urlencoded"
        },
        json: true
      };
      await axios(fetchCall)
        .then(response => {
          // handle success
          console.log("IL RESPONSO 2 single page");

          console.log(response);
          console.log(response.data);
          console.log("hydra:member-----");
          console.log(response.data["hydra:member"]);

          var fetchedData = this.state.fetchSingleFieldData;
          if (response.data["hydra:member"].length !== 0) {
            fetchedData[key] = response.data["hydra:member"];
          }

          this.setState({
            fetchSingleFieldData: fetchedData
          });

          setTimeout(() => {
            // this.setFields();
            // console.log('this.props.location.state.key');
            // console.log(this.props.location.state.key);
            console.log("this.state.fetchSingleFieldData--**");
            console.log(this.state.fetchSingleFieldData);
          }, 500);
        })
        .catch(function(error) {
          // handle error
          console.log(error);
        })
        .finally(function() {
          // always executed
        });
    }
  }

  //finish

  // delete an element
  delete = id => {
    var authToken = this.state.token;
    var fetchCall = {
      method: "DELETE",
      url: `http://api.parmi.io/api/${this.state.key}/${id}`,
      headers: {
        Authorization: "Bearer " + authToken,
        "Content-Type": "application/x-www-form-urlencoded"
      },
      json: true
    };
    axios(fetchCall)
      .then(response => {
        // handle success
        this.fetchData(this.props.location.state.key);
        this.hideDeletePopup();
      })
      .catch(function(error) {
        // handle error
        console.log(error);
      })
      .finally(function() {
        // always executed
      });
  };

  // handle delete popup
  showDeletePopup(id) {
    this.setState({
      deletePopup: true,
      idElementToDelete: id
    });
  }
  hideDeletePopup() {
    this.setState({
      deletePopup: false
    });
  }

  // set up the key in the state of the component for future usage and then
  // call login check
  async setKey() {
    var data = await this.props.location.state.key;
    this.setState({
      key: data
    });
  }

  //++++++top searchbar
  handleChangeSearch(event) {
    this.setState({ searchVal: event.target.value });
    setTimeout(() => {
      console.log(this.state.searchVal);
      if (this.state.searchVal.length === 0) {
        this.fetchData(this.props.location.state.key);
        this.setState({ searchVal: "", highlighEnabled: false });
      }
    }, 20);
  }

  emptySearch() {
    this.setState({ searchVal: "", highlighEnabled: false });
    this.fetchData(this.props.location.state.key);
  }

  handleSubmitSearch(event) {
    console.log("Hai cercato: " + this.state.searchVal);
    event.preventDefault();

    var matchedValues = [];

    this.state.fetchedData.forEach(val => {
      for (const key in val) {
        var string = val[key].toString().toUpperCase();
        var substring = this.state.searchVal.toUpperCase();
        if (string.includes(substring) && !matchedValues.includes(val)) {
          matchedValues.push(val);
          this.setState({
            // matchResultSearch: matchedValues
            fetchedData: matchedValues,
            highlighEnabled: true
          });
        }
      }
    });
    setTimeout(() => {
      console.log("++risultato ricerca++");
      console.log(this.state.fetchedData);
    }, 20);
  }

  sort(field) {
    var sortedFetchedData;
    var data = this.state.fetchedData;

    // I need to transform some data to lowercase(or uppercase) because in unicode sorting,
    // upper and lower case are separated in sorting.
    // FOR NOW I DONT USE IT

    // data.forEach((el) => {
    //   for (const key in el) {
    //     console.log('el[key]');
    //     console.log(el[key]);
    //     if (key === 'nickname') {
    //       el[key] = el[key].toLowerCase();
    //       console.log('el[key]');
    //       console.log(el[key]);
    //     }
    //   }
    // })
    //----

    this.setState({
      showFilterArrow: []
    });

    this.setState(prevState => ({
      showFilterArrow: {
        // object that we want to update
        ...prevState.showFilterArrow, // keep all other key-value pairs
        [field]: true // update the value of specific key
      }
    }));

    if (this.state.filterCounter === 0) {
      this.setState({
        filterCounter: 1,
        renderToggle: false
      });
      console.log("sorted");
      sortedFetchedData = _.orderBy(data, field, ["asc"]);
      console.log("sortedFetchedData");
      console.log(sortedFetchedData);
      this.setState({
        fetchedData: sortedFetchedData
      });
      setTimeout(() => {
        this.setState({
          renderToggle: true
        });
      }, 500);
    } else if (this.state.filterCounter === 1) {
      this.setState({
        filterCounter: 0,
        renderToggle: false
      });
      console.log("sorted");
      sortedFetchedData = _.orderBy(data, field, ["desc"]);
      console.log("sortedFetchedData");
      console.log(sortedFetchedData);
      this.setState({
        fetchedData: sortedFetchedData
      });
      setTimeout(() => {
        this.setState({
          renderToggle: true
        });
      }, 500);
    }
  }

  // set up the fields of the selected table ++++ NOT USED
  // TODO:
  // sarebbe comodo avere un unica tabella html, renderizzando con cicli con map(),
  // ma poi diventa tutto poco customizzabile. preferisco scrivere piu codice ma renderlo
  // piu versatile adattandolo a ogni tabella
  setFields() {
    var fields = [];
    var regex = /@id|@type/g;

    this.state.fetchedData.forEach(el => {
      for (const key in el) {
        if (!fields.includes(key) && _.isEmpty(key.match(regex))) {
          // Setting the fields array and the array with the type of every field
          fields.push(key);
        }
      }
    });
    this.setState({
      fields: fields
    });

    setTimeout(() => {
      console.log("this.state.fields-------");
      console.log(this.state.fields);
      console.log(this.state.fetchedData);
    }, 100);
  }

  // axios call to submit toggle data [patch]
  submitEditToggle(event, id) {
    var authToken = this.state.token;
    var data = {
      correct: event.target.checked
    };
    console.log("==============data======================");
    console.log(data);
    console.log(event.target.checked);

    console.log("====================================");
    axios
      .patch(
        `https://api.parmi.io/api/${this.props.location.state.key}/${id}`,
        JSON.stringify(data),
        {
          headers: {
            Authorization: "Bearer " + authToken,
            accept: "application/ld+json",
            "Content-Type": "application/merge-patch+json"
          }
        }
      )
      .then(response => {
        // handle success
        console.log("----response---");
        console.log(response);

        // oltre a fare la chiamata devo anche cambiare lo stato
        var fetchedData = this.state.fetchedData;
        fetchedData.forEach(f => {
          console.log("==================myelement==================");
          console.log(f["id"]);
          console.log(id);

          console.log("====================================");
          if (id === f["id"]) {
            f["correct"] = !f["correct"];
            console.log("==================weee==================");
            console.log(f["correct"]);
            console.log("====================================");
          }
        });
        this.setState({
          fetchedData: fetchedData
        });
      })
      .catch(function(error) {
        // handle error
        console.log(error);
      })
      .finally(function() {
        // always executed
        console.log("data+++++++");
        console.log(data);
        // console.log(this.props.location.state.id);
      });
  }

  // this method returns the component 'edit btn'
  renderEditBtn(element) {
    var editBtn = (
      <div className="rowTextElement">
        <Link
          className="action btnEdit"
          to={{
            pathname: `${this.state.key}/edit/${element["id"]}`,
            state: {
              title: this.state.title,
              token: this.state.token,
              key: this.state.key,
              fetchedData: this.state.fetchedData,
              id: element["id"]
            }
          }}
        >
          view/edit
        </Link>
      </div>
    );
    return editBtn;
  }

  async refactorUserAnswerTabFields() {
    if (this.props.location.state.key === "user_answers") {
      await this.fetchSingleFieldData("cta_lists").then(() => {
        var ctaLists = this.state.fetchSingleFieldData["cta_lists"];
        console.log("====================================");
        console.log(ctaLists);
        console.log("====================================");
        var fetchedData = this.state.fetchedData;
        console.log("MyfetchedData");
        console.log(fetchedData);
        fetchedData.forEach(f => {
          ctaLists.forEach(c => {
            if (c["@id"] === f["ctaList"]) {
              f["ctaList_name"] = c["name"];
              if (c["answerOption"].length > 1)
                f["ctaList_answerOption"] = c["answerOption"][f["userOption"]];
            }
          });
        });
        console.log("MyfetchedDataedited");
        console.log(fetchedData);
      });

      await this.fetchSingleFieldData("users").then(() => {
        var usersList = this.state.fetchSingleFieldData["users"];
        console.log("====================================");
        console.log(usersList);
        console.log("====================================");

        var fetchedData = this.state.fetchedData;

        fetchedData.forEach(f => {
          usersList.forEach(c => {
            if (c["@id"] === f["User"]) {
              f["user_name"] = c["nickname"];
            }
          });
        });
        console.log("MyfetchedDataedited2");
        console.log(fetchedData);
      });

      this.setState({
        dataLoaded: true
      });

      setTimeout(() => {
        this.setState({
          renderToggle: true
        });
      }, 500);
      return;
    }
  }

  componentDidMount() {
    this.setKey().then(() =>
      this.login().then(
        async () => await this.fetchData(this.props.location.state.key)
      )
    );
  }

  // RENDER
  render() {
    return (
      <div className="elementsBox">
        {this.state.deletePopup ? (
          <div className="deleteConfirmPopup">
            Sicuro di voler cancellare l'elemento con id{" "}
            {this.state.idElementToDelete}?
            <div className="deleteConfirmBox">
              <div
                className="deleteConfirmBtn"
                onClick={() => this.delete(this.state.idElementToDelete)}
              >
                si
              </div>
              <div className="deleteConfirmBtn" onClick={this.hideDeletePopup}>
                no
              </div>
            </div>
          </div>
        ) : null}

        {/* top search */}
        {this.state.loadSearchBar ? (
          <div className="topSearch">
            <div className="tableTitle">{this.state.title}</div>
            <form onSubmit={this.handleSubmitSearch}>
              <label className="tableSearchBar">
                {this.state.searchVal.length > 0 ? (
                  <div className="cancelSearch">
                    <FontAwesomeIcon
                      icon={faWindowClose}
                      className="addBtn"
                      onClick={this.emptySearch}
                    />
                  </div>
                ) : null}
                <input
                  type="text"
                  name="nome"
                  className="inputField"
                  value={this.state.searchVal}
                  onChange={this.handleChangeSearch}
                />
                <input type="submit" value="Search" className="submitBtn" />
              </label>
            </form>
          </div>
        ) : null}
        {this.state.dataLoaded === true && this.state.key !== "user_answers" ? (
          <Link
            className="addNewBtn"
            to={{
              pathname: `${this.state.key}/create`,
              state: {
                title: this.state.title,
                token: this.state.token,
                key: this.state.key,
                fetchedData: this.state.fetchedData
              }
            }}
          >
            Add new {this.state.key}
            <FontAwesomeIcon icon={faPlus} className="addBtn" />
          </Link>
        ) : null}

        {/*CATEGORIES table*/}
        {this.props.location.state.key === "categories" ? (
          <div className="tableBox">
            {this.state.dataLoaded === true ? (
              <div className="singleRow header">
                <h1
                  className="rowTextElement headerElement"
                  onClick={() => this.sort("id")}
                >
                  id{" "}
                  <span>
                    {this.state.showFilterArrow["id"] ? (
                      <FontAwesomeIcon
                        icon={
                          this.state.filterCounter === 0
                            ? faArrowUp
                            : faArrowDown
                        }
                        className="filterArrow"
                      />
                    ) : null}
                  </span>
                </h1>
                <h1
                  className="rowTextElement headerElement"
                  onClick={() => this.sort("ctaLists")}
                >
                  cta list{" "}
                  <span>
                    {this.state.showFilterArrow["ctaLists"] ? (
                      <FontAwesomeIcon
                        icon={
                          this.state.filterCounter === 0
                            ? faArrowUp
                            : faArrowDown
                        }
                        className="filterArrow"
                      />
                    ) : null}
                  </span>
                </h1>
                <h1
                  className="rowTextElement headerElement"
                  onClick={() => this.sort("name")}
                >
                  name{" "}
                  <span>
                    {this.state.showFilterArrow["name"] ? (
                      <FontAwesomeIcon
                        icon={
                          this.state.filterCounter === 0
                            ? faArrowUp
                            : faArrowDown
                        }
                        className="filterArrow"
                      />
                    ) : null}
                  </span>
                </h1>
                <h1 className="rowTextElement headerElement">type</h1>
                <h1 className="rowTextElement headerElement">actions</h1>
                <h1 className="rowTextElement headerElement"> </h1>
              </div>
            ) : null}
            {this.state.dataLoaded === false ? (
              <div className="spinnerBox">
                <Spinner color="" className="spinner" />
              </div>
            ) : (
              this.state.fetchedData.map((element, index) => (
                <React.Fragment key={index}>
                  <div key={index} className="singleRow">
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement">{element["id"]}</h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={element["id"].toString()}
                        className="rowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement">
                        {element["ctaLists"] !== undefined
                          ? element["ctaLists"].length
                          : ""}
                      </h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={element["ctaLists"].length.toString()}
                        className="rowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement">{element["name"]}</h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={element["name"].toString()}
                        className="rowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement">{element["@type"]}</h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={element["@type"].toString()}
                        className="rowTextElement"
                      />
                    )}
                    {/* edit button */}
                    {this.renderEditBtn(element)}
                    <div className=" rowTextElement btnDelete">
                      <span
                        className="action"
                        onClick={() => this.showDeletePopup(element["id"])}
                      >
                        delete
                      </span>
                    </div>
                  </div>
                </React.Fragment>
              ))
            )}
          </div>
        ) : null}

        {/*CTA LISTS table*/}
        {this.props.location.state.key === "cta_lists" ? (
          <div className="tableBox">
            {this.state.dataLoaded === true ? (
              <div className="singleRow header">
                <h1
                  className="rowTextElement headerElement"
                  onClick={() => this.sort("id")}
                >
                  id{" "}
                  <span>
                    {this.state.showFilterArrow["id"] ? (
                      <FontAwesomeIcon
                        icon={
                          this.state.filterCounter === 0
                            ? faArrowUp
                            : faArrowDown
                        }
                        className="filterArrow"
                      />
                    ) : null}
                  </span>
                </h1>
                <h1
                  className="rowTextElement headerElement"
                  onClick={() => this.sort("name")}
                >
                  name{" "}
                  <span>
                    {this.state.showFilterArrow["name"] ? (
                      <FontAwesomeIcon
                        icon={
                          this.state.filterCounter === 0
                            ? faArrowUp
                            : faArrowDown
                        }
                        className="filterArrow"
                      />
                    ) : null}
                  </span>
                </h1>
                <h1 className="rowTextElement headerElement">type</h1>
                <h1 className="rowTextElement headerElement">actions</h1>
                <h1 className="rowTextElement headerElement"> </h1>
              </div>
            ) : null}
            {this.state.dataLoaded === false ? (
              <div className="spinnerBox">
                <Spinner color="" className="spinner" />
              </div>
            ) : (
              this.state.fetchedData.map((element, index) => (
                <React.Fragment key={index}>
                  <div key={index} className="singleRow">
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement">{element["id"]}</h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={element["id"].toString()}
                        className="rowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement">{element["name"]}</h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={element["name"].toString()}
                        className="rowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement">{element["@type"]}</h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={element["@type"].toString()}
                        className="rowTextElement"
                      />
                    )}
                    {/* edit button */}
                    {this.renderEditBtn(element)}
                    <div className=" rowTextElement btnDelete">
                      <span
                        className="action"
                        onClick={() => this.showDeletePopup(element["id"])}
                      >
                        delete
                      </span>
                    </div>
                  </div>
                </React.Fragment>
              ))
            )}
          </div>
        ) : null}

        {/*USERS table*/}
        {this.props.location.state.key === "users" ? (
          <div className="tableBox">
            {this.state.dataLoaded === true ? (
              <div className="singleRow header">
                <h1
                  className="rowTextElement headerElement smallRowTextElement"
                  onClick={() => this.sort("id")}
                >
                  id{" "}
                  <span>
                    {this.state.showFilterArrow["id"] ? (
                      <FontAwesomeIcon
                        icon={
                          this.state.filterCounter === 0
                            ? faArrowUp
                            : faArrowDown
                        }
                        className="filterArrow"
                      />
                    ) : null}
                  </span>
                </h1>
                <h1
                  className="rowTextElement headerElement bigRowTextElement"
                  onClick={() => this.sort("nickname")}
                >
                  nickname{" "}
                  <span>
                    {this.state.showFilterArrow["nickname"] ? (
                      <FontAwesomeIcon
                        icon={
                          this.state.filterCounter === 0
                            ? faArrowUp
                            : faArrowDown
                        }
                        className="filterArrow"
                      />
                    ) : null}
                  </span>
                </h1>
                <h1
                  className="rowTextElement headerElement bigRowTextElement"
                  onClick={() => this.sort("username")}
                >
                  username{" "}
                  <span>
                    {this.state.showFilterArrow["username"] ? (
                      <FontAwesomeIcon
                        icon={
                          this.state.filterCounter === 0
                            ? faArrowUp
                            : faArrowDown
                        }
                        className="filterArrow"
                      />
                    ) : null}
                  </span>
                </h1>
                <h1
                  className="rowTextElement headerElement"
                  onClick={() => this.sort("points")}
                >
                  points{" "}
                  <span>
                    {this.state.showFilterArrow["points"] ? (
                      <FontAwesomeIcon
                        icon={
                          this.state.filterCounter === 0
                            ? faArrowUp
                            : faArrowDown
                        }
                        className="filterArrow"
                      />
                    ) : null}
                  </span>
                </h1>
                <h1 className="rowTextElement headerElement ">is active</h1>
                <h1 className="rowTextElement headerElement mediumRowTextElement">
                  rank
                </h1>
                <h1 className="rowTextElement headerElement">
                  delivered rewards
                </h1>
                <h1 className="rowTextElement headerElement">actions</h1>
                <h1 className="rowTextElement headerElement"> </h1>
              </div>
            ) : null}
            {this.state.dataLoaded === false ? (
              <div className="spinnerBox">
                <Spinner color="" className="spinner" />
              </div>
            ) : (
              this.state.fetchedData.map((element, index) => (
                <React.Fragment key={index}>
                  <div key={index} className="singleRow">
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement smallRowTextElement">
                        {element["id"]}
                      </h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={element["id"].toString()}
                        className="rowTextElement smallRowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement bigRowTextElement">
                        {element["nickname"]}
                      </h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={element["nickname"].toString()}
                        className="rowTextElement bigRowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement bigRowTextElement">
                        {element["username"]}
                      </h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={element["username"].toString()}
                        className="rowTextElement bigRowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement">{element["points"]}</h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={element["points"].toString()}
                        className="rowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement">{element["isActive"]}</h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={element["isActive"].toString()}
                        className="rowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement mediumRowTextElement">
                        {element["rank"]}
                      </h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={element["rank"].toString()}
                        className="rowTextElement mediumRowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement">
                        {element["deliveredRewards"]}
                      </h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={element["deliveredRewards"].toString()}
                        className="rowTextElement"
                      />
                    )}
                    {/* edit button */}
                    {this.renderEditBtn(element)}
                    <div className=" rowTextElement btnDelete">
                      <span
                        className="action"
                        onClick={() => this.showDeletePopup(element["id"])}
                      >
                        delete
                      </span>
                    </div>
                  </div>
                </React.Fragment>
              ))
            )}
          </div>
        ) : null}

        {/*C ANSWERS table*/}
        {this.props.location.state.key === "c_answers" ? (
          <div className="tableBox">
            {this.state.dataLoaded === true ? (
              <div className="singleRow header">
                <h1
                  className="rowTextElement headerElement smallRowTextElement"
                  onClick={() => this.sort("id")}
                >
                  id{" "}
                  <span>
                    {this.state.showFilterArrow["id"] ? (
                      <FontAwesomeIcon
                        icon={
                          this.state.filterCounter === 0
                            ? faArrowUp
                            : faArrowDown
                        }
                        className="filterArrow"
                      />
                    ) : null}
                  </span>
                </h1>
                <h1
                  className="rowTextElement headerElement bigRowTextElement"
                  onClick={() => this.sort("wordUser")}
                >
                  word user{" "}
                  <span>
                    {this.state.showFilterArrow["wordUser"] ? (
                      <FontAwesomeIcon
                        icon={
                          this.state.filterCounter === 0
                            ? faArrowUp
                            : faArrowDown
                        }
                        className="filterArrow"
                      />
                    ) : null}
                  </span>
                </h1>
                <h1 className="rowTextElement headerElement bigRowTextElement">
                  user option
                </h1>
                <h1 className="rowTextElement headerElement">approved</h1>
                <h1 className="rowTextElement headerElement ">correct</h1>
                <h1 className="rowTextElement headerElement mediumRowTextElement">
                  user
                </h1>
                <h1 className="rowTextElement headerElement">cta list</h1>
                <h1 className="rowTextElement headerElement">actions</h1>
                <h1 className="rowTextElement headerElement"> </h1>
              </div>
            ) : null}
            {this.state.dataLoaded === false ? (
              <div className="spinnerBox">
                <Spinner color="" className="spinner" />
              </div>
            ) : (
              this.state.fetchedData.map((element, index) => (
                <React.Fragment key={index}>
                  <div key={index} className="singleRow">
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement smallRowTextElement">
                        {element["id"]}
                      </h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={element["id"].toString()}
                        className="rowTextElement smallRowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement bigRowTextElement">
                        {element["wordUser"]}
                      </h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={
                          element["wordUser"].toString() !== undefined
                            ? element["wordUser"].toString()
                            : null
                        }
                        className="rowTextElement bigRowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement bigRowTextElement">
                        {element["userOption"]}
                      </h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={
                          element["userOption"] !== undefined
                            ? element["userOption"].toString()
                            : null
                        }
                        className="rowTextElement bigRowTextElement"
                      />
                    )}
                    {/* TODO: */}
                    {/* implementare i valori nulli approved, correct, user */}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement">{element["approved"]}</h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={""}
                        className="rowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement">{element["correct"]}</h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={""}
                        className="rowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement mediumRowTextElement">
                        {element["user"]}
                      </h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={""}
                        className="rowTextElement mediumRowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement">{element["ctaList"]}</h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={element["ctaList"].toString()}
                        className="rowTextElement"
                      />
                    )}
                    {/* edit button */}
                    {this.renderEditBtn(element)}
                    <div className=" rowTextElement btnDelete">
                      <span
                        className="action"
                        onClick={() => this.showDeletePopup(element["id"])}
                      >
                        delete
                      </span>
                    </div>
                  </div>
                </React.Fragment>
              ))
            )}
          </div>
        ) : null}

        {/*USER ANSWERS table*/}
        {this.props.location.state.key === "user_answers" ? (
          <div className="tableBox">
            {this.state.dataLoaded === true ? (
              <div className="singleRow header">
                <h1
                  className="rowTextElement headerElement smallRowTextElement"
                  onClick={() => this.sort("id")}
                >
                  id{" "}
                  <span>
                    {this.state.showFilterArrow["id"] ? (
                      <FontAwesomeIcon
                        icon={
                          this.state.filterCounter === 0
                            ? faArrowUp
                            : faArrowDown
                        }
                        className="filterArrow"
                      />
                    ) : null}
                  </span>
                </h1>
                <h1
                  className="rowTextElement headerElement bigRowTextElementUserAnswers"
                  onClick={() => this.sort("wordUser")}
                >
                  word user{" "}
                  <span>
                    {this.state.showFilterArrow["wordUser"] ? (
                      <FontAwesomeIcon
                        icon={
                          this.state.filterCounter === 0
                            ? faArrowUp
                            : faArrowDown
                        }
                        className="filterArrow"
                      />
                    ) : null}
                  </span>
                </h1>
                <h1 className="rowTextElement headerElement mediumRowTextElement">
                  user option
                </h1>
                <h1 className="rowTextElement headerElement mediumRowTextElement">
                  imageName
                </h1>

                <h1 className="rowTextElement headerElement">approved</h1>
                <h1 className="rowTextElement headerElement ">correct</h1>
                <h1 className="rowTextElement headerElement mediumRowTextElement">
                  user
                </h1>
                <h1
                  className="rowTextElement headerElement"
                  onClick={() => this.sort("ctaList")}
                >
                  cta list
                  <span>
                    {this.state.showFilterArrow["ctaList"] ? (
                      <FontAwesomeIcon
                        icon={
                          this.state.filterCounter === 0
                            ? faArrowUp
                            : faArrowDown
                        }
                        className="filterArrow"
                      />
                    ) : null}
                  </span>
                </h1>
                <h1 className="rowTextElement headerElement">actions</h1>
                <h1 className="rowTextElement headerElement"> </h1>
              </div>
            ) : null}
            {this.state.dataLoaded === false ? (
              <div className="spinnerBox">
                <Spinner color="" className="spinner" />
              </div>
            ) : (
              this.state.fetchedData.map((element, index) => (
                <React.Fragment key={index}>
                  <div key={index} className="singleRow">
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement smallRowTextElement">
                        {element["id"]}
                      </h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={
                          element["id"] !== undefined
                            ? element["id"].toString()
                            : null
                        }
                        className="rowTextElement smallRowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement bigRowTextElementUserAnswers">
                        {element["wordUser"]}
                      </h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={
                          element["wordUser"] !== undefined
                            ? element["wordUser"].toString()
                            : ""
                        }
                        className="rowTextElement bigRowTextElementUserAnswers"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement mediumRowTextElement">
                        {element["ctaList_answerOption"]}
                      </h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={
                          element["ctaList_answerOption"] !== undefined
                            ? element["ctaList_answerOption"].toString()
                            : ""
                        }
                        className="rowTextElement mediumRowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement mediumRowTextElement">
                        {element["imageName"]}
                      </h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={
                          element["imageName"] !== undefined
                            ? element["imageName"].toString()
                            : ""
                        }
                        className="rowTextElement mediumRowTextElement"
                      />
                    )}
                    {/* TODO: */}
                    {/* implementare i valori nulli approved, correct, user */}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement">{element["approved"]}</h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={""}
                        className="rowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <div className="rowTextElement">
                        <h1>{element["correct"]}</h1>

                        {this.state.renderToggle ? (
                          <Toggle
                            defaultChecked={element["correct"]}
                            className="custom-classname"
                            onChange={event =>
                              this.submitEditToggle(event, element["id"])
                            }
                          />
                        ) : null}
                      </div>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={""}
                        className="rowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement mediumRowTextElement">
                        {element["user_name"]}
                      </h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={
                          element["user_name"] !== undefined
                            ? element["user_name"].toString()
                            : ""
                        }
                        className="rowTextElement mediumRowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement">
                        {element["ctaList_name"]}
                      </h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={element["ctaList_name"].toString()}
                        className="rowTextElement"
                      />
                    )}
                    {/* edit button */}
                    {this.renderEditBtn(element)}
                    <div className=" rowTextElement btnDelete">
                      <span
                        className="action"
                        onClick={() => this.showDeletePopup(element["id"])}
                      >
                        delete
                      </span>
                    </div>
                  </div>
                </React.Fragment>
              ))
            )}
          </div>
        ) : null}

        {/*DELIVERED REWARDS table*/}
        {this.props.location.state.key === "delivered_rewards" ? (
          <div className="tableBox">
            {this.state.dataLoaded === true ? (
              <div className="singleRow header">
                <h1
                  className="rowTextElement headerElement smallRowTextElement"
                  onClick={() => this.sort("id")}
                >
                  id{" "}
                  <span>
                    {this.state.showFilterArrow["id"] ? (
                      <FontAwesomeIcon
                        icon={
                          this.state.filterCounter === 0
                            ? faArrowUp
                            : faArrowDown
                        }
                        className="filterArrow"
                      />
                    ) : null}
                  </span>
                </h1>
                <h1
                  className="rowTextElement headerElement bigRowTextElement"
                  onClick={() => this.sort("coupon")}
                >
                  coupon{" "}
                  <span>
                    {this.state.showFilterArrow["coupon"] ? (
                      <FontAwesomeIcon
                        icon={
                          this.state.filterCounter === 0
                            ? faArrowUp
                            : faArrowDown
                        }
                        className="filterArrow"
                      />
                    ) : null}
                  </span>
                </h1>
                <h1 className="rowTextElement headerElement bigRowTextElement">
                  delivered
                </h1>
                <h1 className="rowTextElement headerElement">cta list</h1>
                <h1 className="rowTextElement headerElement ">user</h1>
                <h1 className="rowTextElement headerElement mediumRowTextElement">
                  rewards
                </h1>
                <h1 className="rowTextElement headerElement">actions</h1>
                <h1 className="rowTextElement headerElement"> </h1>
              </div>
            ) : null}
            {this.state.dataLoaded === false ? (
              <div className="spinnerBox">
                <Spinner color="" className="spinner" />
              </div>
            ) : (
              this.state.fetchedData.map((element, index) => (
                <React.Fragment key={index}>
                  <div key={index} className="singleRow">
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement smallRowTextElement">
                        {element["id"]}
                      </h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={element["id"].toString()}
                        className="rowTextElement smallRowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement bigRowTextElement">
                        {element["wordUser"]}
                      </h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={
                          element["wordUser"] !== undefined
                            ? element["wordUser"].toString()
                            : null
                        }
                        className="rowTextElement bigRowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement bigRowTextElement">
                        {element["userOption"]}
                      </h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={
                          element["userOption"] !== undefined
                            ? element["userOption"].toString()
                            : null
                        }
                        className="rowTextElement bigRowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement">{element["approved"]}</h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={""}
                        className="rowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement">{element["correct"]}</h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={""}
                        className="rowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement mediumRowTextElement">
                        {element["user"]}
                      </h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={""}
                        className="rowTextElement mediumRowTextElement"
                      />
                    )}
                    {!this.state.highlighEnabled ? (
                      <h1 className="rowTextElement">{element["ctaList"]}</h1>
                    ) : (
                      <Highlighter
                        highlightClassName=" highlightClass"
                        searchWords={[this.state.searchVal.toString()]}
                        autoEscape={true}
                        textToHighlight={element["ctaList"].toString()}
                        className="rowTextElement"
                      />
                    )}
                    {/* edit button */}
                    {this.renderEditBtn(element)}
                    <div className=" rowTextElement btnDelete">
                      <span
                        className="action"
                        onClick={() => this.showDeletePopup(element["id"])}
                      >
                        delete
                      </span>
                    </div>
                  </div>
                </React.Fragment>
              ))
            )}
          </div>
        ) : null}
      </div>
    );
  }
}
export default translate()(Table);
