import React, { Component } from "react";
import { translate } from "react-i18next";
import "./ctalist.scss";
import { Link } from "react-router-dom";
import { Spinner } from "reactstrap";
import { db } from "../portfolio_single_page/portfolio_sp";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowRight,
  faArrowLeft,
  faArrowAltCircleLeft,
  faWindowClose,
  faPlus
} from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import { BrowserRouter as Router, Route } from "react-router-dom";
import TopSearch from "../topSearch/topSearch";
import {
  useTable,
  useGroupBy,
  useFilters,
  useSortBy,
  useExpanded,
  usePagination
} from "react-table";

class CtaList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "cta lists",
      dataLoaded: false,
      fetchedData: [],
      loadSearchBar: false,
      token: "",
      deletePopup: false,
      idElementToDelete: null
    };
  }

  login() {
    axios
      .post("http://api.parmi.io/login_check", {
        username: "parmi2020@parmi.io",
        password: "parmi"
      })
      .then(({ data }) => {
        console.log("userSignIn: ", data);
        if (data.payload.token) {
          localStorage.setItem("token", JSON.stringify(data.payload.token));
          localStorage.setItem("user", JSON.stringify(data.userdata));
          this.setState({
            token: data.payload.token
          });
          this.fetchData();
        }
      })
      .catch(function(error) {
        console.log("Error****:", error.message);
      });
  }

  async fetchData() {
    var authToken = this.state.token;
    var fetchCall = {
      method: "GET",
      url: "http://api.parmi.io/api/cta_lists",
      headers: {
        Authorization: "Bearer " + authToken,
        "Content-Type": "application/x-www-form-urlencoded"
      },
      json: true
    };
    axios(fetchCall)
      .then(response => {
        // handle success
        console.log(response);
        console.log(response.data);

        this.setState({
          dataLoaded: true,
          fetchedData: response.data["hydra:member"],
          loadSearchBar: true
        });
        this.handleLoading();
      })
      .catch(function(error) {
        // handle error
        console.log(error);
      })
      .finally(function() {
        // always executed
      });
  }

  delete = id => {
    var authToken = this.state.token;
    var fetchCall = {
      method: "DELETE",
      url: `http://api.parmi.io/api/categories/${id}`,
      headers: {
        Authorization: "Bearer " + authToken,
        "Content-Type": "application/x-www-form-urlencoded"
      },
      json: true
    };
    axios(fetchCall)
      .then(response => {
        // handle success

        this.fetchData();
        this.hideDeletePopup();
      })
      .catch(function(error) {
        // handle error
        console.log(error);
      })
      .finally(function() {
        // always executed
      });
  };

  showDeletePopup(id) {
    this.setState({
      deletePopup: true,
      idElementToDelete: id
    });
  }
  hideDeletePopup() {
    this.setState({
      deletePopup: false
    });
  }

  componentDidMount() {
    this.login();
  }

  render() {
    return (
      <div className="elementsBox">
        {this.state.deletePopup ? (
          <div className="deleteConfirmPopup">
            Sicuro di voler cancellare l'elemento con id{" "}
            {this.state.idElementToDelete}?
            <div className="deleteConfirmBox">
              <div
                className="deleteConfirmBtn"
                onClick={() => this.delete(this.state.idElementToDelete)}
              >
                si
              </div>
              <div className="deleteConfirmBtn" onClick={this.hideDeletePopup}>
                no
              </div>
            </div>
          </div>
        ) : null}
        {this.state.loadSearchBar ? (
          <TopSearch
            title={this.state.title}
            fetchedData={this.state.fetchedData}
          ></TopSearch>
        ) : null}
        <div className="tableBox">
          {this.state.dataLoaded === true ? (
            <div className="addNewBtn">
              Create <FontAwesomeIcon icon={faPlus} className="aboutIcon" />
            </div>
          ) : null}
          {this.state.dataLoaded === true ? (
            <div className="singleRow header">
              <h1 className="rowTextElement headerElement">path</h1>
              <h1 className="rowTextElement headerElement">id</h1>
              <h1 className="rowTextElement headerElement">name</h1>
              <h1 className="rowTextElement headerElement">type</h1>
              <h1 className="rowTextElement headerElement">actions</h1>
              <h1 className="rowTextElement headerElement"> </h1>
            </div>
          ) : null}
          {this.state.dataLoaded === false ? (
            <div className="spinnerBox">
              <Spinner color="" className="spinner" />
            </div>
          ) : (
            this.state.fetchedData.map((element, index) => (
              <React.Fragment key={index}>
                <div key={index} className="singleRow">
                  <h1 className="rowTextElement">{element["@id"]}</h1>
                  <h1 className="rowTextElement">{element["id"]}</h1>
                  <h1 className="rowTextElement">{element["name"]}</h1>
                  <h1 className="rowTextElement">{element["@type"]}</h1>
                  <div className="rowTextElement btnEdit">
                    <span className="action">edit</span>
                  </div>
                  <div className=" rowTextElement btnDelete">
                    <span
                      className="action"
                      onClick={() => this.showDeletePopup(element["id"])}
                    >
                      delete
                    </span>
                  </div>
                </div>
              </React.Fragment>
            ))
          )}
        </div>
      </div>
    );
  }
}
export default translate()(CtaList);
