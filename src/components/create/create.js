import React, { Component } from "react";
import { translate } from "react-i18next";
import "./create.scss";
import { Link } from "react-router-dom";
import { Spinner } from "reactstrap";
import { db } from "../portfolio_single_page/portfolio_sp";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowRight,
  faArrowLeft,
  faArrowAltCircleLeft,
  faWindowClose,
  faPlus,
  faChevronRight,
  faCross,
} from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import { BrowserRouter as Router, Route } from "react-router-dom";
import TopSearch from "../topSearch/topSearch";
import {
  useTable,
  useGroupBy,
  useFilters,
  useSortBy,
  useExpanded,
  usePagination,
} from "react-table";
import { test } from "../shared_methods";
import _ from "lodash";
import Select from "react-select";
import Toggle from "react-toggle";
import "react-toggle/style.css";
import qs from "querystring";

class Create extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: this.props.location.state.title,
      key: "",
      fields: [],
      fieldsWithType: [],
      forbiddenInputs: ["@id", "id", "@type", "ctaLists"],
      fieldsToFetchForSelect: ["cta_lists", "categories"],
      fetchDataForSelectInput: [],
      inputValues: [],
      inputErrors: [],
      finalData: [],
      answerOptions: [1],
      toastMessage: "",
      dataLoaded: false,
      fetchedData: [],
      lastID: null,
      loadSearchBar: false,
      token: "",
      deletePopup: false,
      idElementToDelete: null,
      showSubmitToast: false,
      fadeToast: false,
      categoriesList: [],
      hideToggle: [],
      isSingleAnswer: false,
      multipleCorrectAnswerIndex: 0,
      correctAnswerObject: { "1": false, "2": false },
      imageFile: null,
      showFotoInput: false,
    };

    this.textInputHandler = this.textInputHandler.bind(this);
    this.addNewInputField = this.addNewInputField.bind(this);
    this.removeNewInputField = this.removeNewInputField.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  //form handlers=====

  // TEXT INPUT HANDLER
  textInputHandler = (event, field) => {
    // error handling
    var arrLenght = Array.from(event.target.value).length;
    var type;
    for (const key in this.state.fieldsWithType) {
      if (key === field) {
        console.log("this.state.fieldsWithType[key]");
        type = this.state.fieldsWithType[key];
        console.log(type);
      }
    }
    // error based on string length
    if (type === "string") {
      if (arrLenght < 4) {
        this.setState((prevState) => ({
          inputErrors: {
            // object that we want to update
            ...prevState.inputErrors, // keep all other key-value pairs
            [field]: "This field must be at least 4 characters long!", // update the value of specific key
          },
        }));
      } else {
        this.setState((prevState) => ({
          inputErrors: {
            // object that we want to update
            ...prevState.inputErrors, // keep all other key-value pairs
            [field]: null, // update the value of specific key
          },
        }));
      }
      if (arrLenght === 0) {
        this.setState((prevState) => ({
          inputErrors: {
            // object that we want to update
            ...prevState.inputErrors, // keep all other key-value pairs
            [field]: null, // update the value of specific key
          },
        }));
      }
    }
    // error based on number range
    if (type === "number") {
      if (event.target.value < 0) {
        this.setState((prevState) => ({
          inputErrors: {
            // object that we want to update
            ...prevState.inputErrors, // keep all other key-value pairs
            [field]: "Please, insert positive number", // update the value of specific key
          },
        }));
      } else {
        this.setState((prevState) => ({
          inputErrors: {
            // object that we want to update
            ...prevState.inputErrors, // keep all other key-value pairs
            [field]: null, // update the value of specific key
          },
        }));
      }
      if (arrLenght === 0) {
        this.setState((prevState) => ({
          inputErrors: {
            // object that we want to update
            ...prevState.inputErrors, // keep all other key-value pairs
            [field]: null, // update the value of specific key
          },
        }));
      }
    }
    // error based on boolean
    if (type === "boolean") {
      console.log("this.state.inputValues[field]--------");
      console.log(this.state.inputValues[field]);
      console.log(event.target.value);

      if (event.target.value !== "true" && event.target.value !== "false") {
        this.setState((prevState) => ({
          inputErrors: {
            // object that we want to update
            ...prevState.inputErrors, // keep all other key-value pairs
            [field]: "Please, insert boolean", // update the value of specific key
          },
        }));
      } else {
        this.setState((prevState) => ({
          inputErrors: {
            // object that we want to update
            ...prevState.inputErrors, // keep all other key-value pairs
            [field]: null, // update the value of specific key
          },
        }));
      }
      if (arrLenght === 0) {
        this.setState((prevState) => ({
          inputErrors: {
            // object that we want to update
            ...prevState.inputErrors, // keep all other key-value pairs
            [field]: null, // update the value of specific key
          },
        }));
      }
    }
    // error based on string regexp for email
    if (field === "email") {
      if (
        arrLenght < 5 ||
        !event.target.value
          .toLowerCase()
          .match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)
      ) {
        this.setState({
          emailError: "Invalid email",
        });
      } else {
        this.setState({
          emailError: null,
        });
      }
      if (arrLenght === 0) {
        this.setState({
          emailError: null,
        });
      }
    }

    //===handling change of input field
    console.log("====================================");
    console.log(event.target.value);
    console.log(field);
    console.log("====================================");
    var value;

    if (this.setInputType(field) === "number") {
      value = parseInt(event.target.value);
    } else {
      value = event.target.value;
    }
    this.setState((prevState) => ({
      inputValues: {
        // object that we want to update
        ...prevState.inputValues, // keep all other key-value pairs
        [field]: value, // update the value of specific key in [] so it is a variable
      },
    }));

    setTimeout(() => {
      console.log("this.state.inputValues");
      console.log(this.state.inputValues);
      console.log("this.state.inputErrors");
      console.log(this.state.inputErrors);
    }, 100);
  };

  // MULTIPLE FIELDS HANDLER

  multipleTextInputHandler = (event, field, index) => {
    // case of multiple input
    // answerOptions
    console.log("====================================");
    console.log(event.target.value);
    console.log("====================================");
    if (this.state.inputValues[field]) {
      var values = this.state.inputValues[field];
    } else {
      values = [];
    }
    values[index] = event.target.value;
    this.setState((prevState) => ({
      inputValues: {
        // object that we want to update
        ...prevState.inputValues, // keep all other key-value pairs
        [field]: values, // update the value of specific key in [] so it is a variable
      },
    }));

    setTimeout(() => {
      console.log("this.state.inputValues");
      console.log(this.state.inputValues);
      console.log("this.state.inputErrors");
      console.log(this.state.inputErrors);
    }, 100);
  };

  //TOGGLE HANDLER
  toggleInputHandler = (event, field) => {
    //===handling change of toggle field
    console.log("====================================");
    console.log(event.target.checked);
    console.log(field);
    console.log("====================================");
    var value = event.target.checked;
    this.setState((prevState) => ({
      inputValues: {
        // object that we want to update
        ...prevState.inputValues, // keep all other key-value pairs
        [field]: value, // update the value of specific key in [] so it is a variable
      },
    }));

    setTimeout(() => {
      console.log(this.state.inputValues);
      console.log(this.state.inputErrors);
    }, 100);
  };

  //SELECT HANDLER
  selectInputHandler = (event, field) => {
    //===handling change of toggle field
    console.log("====================================");
    console.log(event);
    console.log(field);
    console.log("====================================");
    var value = event;

    this.setState({
      showFotoInput: false,
    });

    this.setState((prevState) => ({
      inputValues: {
        // object that we want to update
        ...prevState.inputValues, // keep all other key-value pairs
        [field]: value, // update the value of specific key in [] so it is a variable
      },
    }));

    setTimeout(() => {
      // this method changes the input ui according to the select type
      this.handleSelectType();
      if (this.state.inputValues["type"]["value"] === "Foto") {
        this.setState({
          showFotoInput: true,
        });
      }
      console.log("this.state.inputValuesEEEEEE");
      console.log(this.state.inputValues["type"]["value"]);

      console.log(this.state.inputErrors);
    }, 100);
  };

  // ----------SUBMIT----------

  //handle frontend submit
  handleSubmit = (event) => {
    // TODO:
    // method called here only to mock data bypassing the error
    // checks
    this.finalDataJson();
    setTimeout(() => {
      this.submitToApi();
    }, 1000);

    // Calculating the lenght of the objects of values and errors
    var inputErrorsSize = 0;
    for (const key in this.state.inputErrors) {
      if (this.state.inputErrors[key] !== null) {
        inputErrorsSize += 1;
      }
    }
    var inputValuesSize = 0;
    for (const key in this.state.inputValues) {
      if (
        this.state.inputValues[key] !== null &&
        this.state.inputValues[key] !== ""
      ) {
        inputValuesSize += 1;
      }
    }
    // Calculating the actual fields length, without hidden fields!
    var actualFieldsFiltered = this.state.fields.filter(
      (field) => this.state.forbiddenInputs.includes(field) === false
    );

    console.log("====================lengths================");
    console.log(inputErrorsSize);
    console.log(inputValuesSize);
    console.log(actualFieldsFiltered.length);
    console.log(actualFieldsFiltered);
    console.log("====================================");

    if (
      inputErrorsSize === 0 &&
      inputValuesSize === actualFieldsFiltered.length
    ) {
      // TODO:
      //this is the real submit method, not the one above

      // this.finalDataJson();
      // setTimeout(() => {
      // this.submitToApi();
      // }, 1000);
      this.setState({
        showSubmitToast: true,
        toastMessage: "Element successfully added!",
        inputValues: [],
      });
      setTimeout(() => {
        this.setState({ fadeToast: true });
      }, 1500);
      setTimeout(() => {
        this.setState({ showSubmitToast: false, fadeToast: false });
      }, 3500);
    } else {
      this.setState({
        showSubmitToast: true,
        toastMessage: "Error",
      });
      setTimeout(() => {
        this.setState({ fadeToast: true });
      }, 1500);
      setTimeout(() => {
        this.setState({ showSubmitToast: false, fadeToast: false });
      }, 3500);
    }

    event.preventDefault();
  };

  // TODO:
  // We need to update the final data, to let them be accepted by the backend
  finalDataJson() {
    var inputData = this.state.inputValues;
    var finalData = this.state.inputValues;

    console.log("finalData");
    console.log(finalData);

    for (const key in finalData) {
      console.log("key");
      console.log(key);

      if (key === "ctaLists") {
        // for (const val in inputData[key]) {
        //   console.log('STAMPA');
        //   console.log(inputData[key][val]['value']);
        //   finalData[key].splice(finalData[key], 1, inputData[key][val]['value']);
        // }
        for (let i = 0; i < inputData[key].length; i++) {
          console.log("STAMPA");
          console.log(inputData[key][i]["value"]);
          finalData[key].splice(
            finalData[key][i],
            1,
            inputData[key][i]["value"]
          );
        }
      }

      // it removes possible empty, null or "" value in the answer option
      if (key === "answerOption") {
        finalData[key] = inputData[key];
        _.remove(finalData[key], function (n) {
          return n === "" || n === null || n === undefined;
        });
      }

      // it switches the category name with the path: /api/categories/{id}
      if (key === "Category") {
        var categoriesNamesInput = [];
        var categoriesInput = inputData[key];
        for (let i = 0; i < categoriesInput.length; i++) {
          console.log("cat------");
          console.log(categoriesInput[i].value);
          categoriesNamesInput.push(categoriesInput[i].value);
        }
        console.log("categoriesNamesInput");
        console.log(categoriesNamesInput);
        var listaCategorie = this.state.categoriesList;
        for (let i = 0; i < listaCategorie.length; i++) {
          console.log("listaCategorie[i][]");
          console.log(listaCategorie[i]["name"]);

          if (_.includes(categoriesNamesInput, listaCategorie[i]["name"])) {
            console.log("trovato");

            finalData[key].push(listaCategorie[i]["@id"]);
          }
        }
        _.remove(finalData[key], function (n) {
          return typeof n !== "string";
        });

        console.log("finalData[key].....");
        console.log(finalData[key]);
      }
    }

    if (this.props.location.state.key === "cta_lists") {
      console.log("FINALE2");
      // console.log(finalData);
      if (finalData["type"] === undefined || finalData["type"] === null) {
        finalData["type"] = 0;
      } else {
        if (finalData["type"]["value"] === "Risposta Multipla") {
          finalData["type"] = 0;
        }
        if (finalData["type"]["value"] === "Parola") {
          finalData["type"] = 1;
        }
        if (finalData["type"]["value"] === "Foto") {
          finalData["type"] = 2;
        }
      }
    }

    this.setState({
      finalData: finalData,
    });
    setTimeout(() => {
      console.log("finalData-------");
      console.log(finalData);
    }, 1000);
  }

  // fetch categories (helper method to switch the category name with path)

  async fetchCategories() {
    var authToken = this.props.location.state.token;

    var fetchCall = {
      method: "GET",
      url: `http://api.parmi.io/api/categories`,
      headers: {
        Authorization: "Bearer " + authToken,
        "Content-Type": "application/x-www-form-urlencoded",
      },
      json: true,
    };
    await axios(fetchCall)
      .then((response) => {
        // handle success
        console.log("----response---");
        console.log(response.data);
        console.log(response.data["hydra:member"]);
        this.setState({
          categoriesList: response.data["hydra:member"],
        });
        console.log("this.state.categoriesList----");
        console.log(this.state.categoriesList);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .finally(function () {
        // always executed
      });
  }

  // axios call to submit data

  async submitToApi() {
    var authToken = this.props.location.state.token;

    // var data =
    // {
    //   "name": "test_1",
    //   "description": "string",
    //   "Category": ["api/categories/25"],
    //   "points": 0,
    //   "Question": "string",
    //   "answerOption": [
    //     "string"
    //   ],
    //   "type": 0,
    //   "ctaRank": 0
    // }

    var data = this.state.finalData;
    // var data = this.state.inputValues;

    axios
      .post(
        `https://api.parmi.io/api/${this.props.location.state.key}`,
        JSON.stringify(data),
        {
          headers: {
            Authorization: "Bearer " + authToken,
            accept: "application/ld+json",
            "Content-Type": "application/ld+json",
          },
        }
      )
      .then((response) => {
        // handle success
        console.log("----response--id-");
        console.log(response);
        console.log(response.data["id"]);

        // if cta , i submit also the correct answer
        this.submitCorrectAnswerToApi(response.data["id"]);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .finally(function () {
        // always executed
        console.log("data+++++++");
        console.log(data);
        console.log("data to string+++++++");
        console.log(JSON.stringify(data));
      });
  }

  async submitCorrectAnswerToApi(id) {
    if (this.props.location.state.key === "cta_lists") {
      var authToken = this.props.location.state.token;

      // var data =
      // {
      //   "name": "test_1",
      //   "description": "string",
      //   "Category": ["api/categories/25"],
      //   "points": 0,
      //   "Question": "string",
      //   "answerOption": [
      //     "string"
      //   ],
      //   "type": 0,
      //   "ctaRank": 0
      // }

      var data = this.state.finalData;
      console.log("iDatifinali");
      console.log(data);
      var dataForCorrectAnswer;

      if (this.state.finalData["type"] === 0) {
        dataForCorrectAnswer = {
          TypeId: 0,
          correctedOption: this.state.multipleCorrectAnswerIndex,
          ctaListId: id,
        };
      }
      if (this.state.finalData["type"] === 1) {
        dataForCorrectAnswer = {
          TypeId: 1,
          word: this.state.finalData["answerOption"][0],
          ctaListId: id,
        };
      }

      console.log("se la risp è singola, la risposta è---->:");

      console.log(this.state.finalData["answerOption"][0]);

      console.log("dataForCorrectAnswer");
      console.log(dataForCorrectAnswer);
      console.log(
        `the correct answer is ${
          this.state.finalData["answerOption"][
            this.state.multipleCorrectAnswerIndex
          ]
        }`
      );

      axios
        .post(
          `https://api.parmi.io/api/c_answers`,
          JSON.stringify(dataForCorrectAnswer),
          {
            headers: {
              Authorization: "Bearer " + authToken,
              accept: "application/ld+json",
              "Content-Type": "application/ld+json",
            },
          }
        )
        .then((response) => {
          // handle success
          console.log("----response--canswee-");
          console.log(response);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .finally(function () {
          // always executed
          console.log("data+++++++correct answer");
          console.log(dataForCorrectAnswer);
        });
    }
  }

  // set correct answer checkbox fo multiple answer

  setCorrectAnswerCheckbox(event, index) {
    var multipleCorrectAnswerIndex;
    var correctAnswerObject = this.state.correctAnswerObject;
    var value = event.target.checked;
    var answerOptions = this.state.answerOptions;

    console.log("answerOptions");
    console.log(answerOptions);

    if (value === true) {
      for (const key in correctAnswerObject) {
        if (key !== index) {
          correctAnswerObject[key] = false;
        } else {
          correctAnswerObject[key] = true;
        }

        multipleCorrectAnswerIndex = _.indexOf(answerOptions, index);
        console.log("indice");
        console.log(multipleCorrectAnswerIndex);
      }
    }

    console.log("event.target.checked");
    console.log(value);
    this.setState((prevState) => ({
      multipleCorrectAnswerIndex: multipleCorrectAnswerIndex,
      correctAnswerObject: {
        // object that we want to update
        ...prevState.correctAnswerObject, // keep all other key-value pairs
        [index]: value, // update the value of specific key in [] so it is a variable
      },
    }));

    setTimeout(() => {
      console.log("this.state.correctAnswerObject");
      console.log(this.state.correctAnswerObject);
      console.log("this.state.multipleCorrectAnswerIndex");
      console.log(this.state.multipleCorrectAnswerIndex);
    }, 500);
  }

  //=======

  // set up the key in the state of the component for future usage and then
  // call login check
  async setFetchedData() {
    var data = this.props.location.state.fetchedData;
    var lastID;
    if (!_.isEmpty(data)) {
      lastID = _.last(data)["id"];
    }
    this.setState({
      fetchedData: data,
      lastID: lastID,
    });

    setTimeout(() => {
      console.log("this.state.fetchedData------+++");

      console.log(this.state.fetchedData);
      console.log("----LAST----");
      console.log(this.state.lastID);

      this.setFields();
    }, 100);
  }

  // set up the fields of the selected table
  setFields() {
    var fields = [];
    var fieldsWithType = [];

    this.state.fetchedData.forEach((el) => {
      for (const key in el) {
        if (!fields.includes(key) && !fieldsWithType.includes(key)) {
          // Setting the fields array and the array with the type of every field
          fields.push(key);
          fieldsWithType[key] = typeof el[key];
        }
      }
    });
    this.setState({
      fields: fields,
      fieldsWithType: fieldsWithType,
    });

    setTimeout(() => {
      console.log("this.state.fields-------");
      console.log(this.state.fields);
      console.log(this.state.fieldsWithType);
    }, 1000);
  }

  // precompile input id or set default value for all inputs
  setInputValue(field) {
    switch (field) {
      case "id":
        return this.state.lastID + 1;
      case "@id":
        return `/api/${this.props.location.state.key}/${this.state.lastID + 1}`;
      default:
        return this.state.inputValues.field;
    }
  }

  //********** set type of input: here i can CONTROL THE INPUT TYPE FOR EACH FIELD ***********//
  setInputType(field) {
    if (field === "id" || field === "ctaRank" || field === "points") {
      return "number";
    }
    if (field === "email") {
      return "email";
    }
    if (
      field === "ctaList" ||
      field === "ctaLists" ||
      field === "Category" ||
      field === "type"
    ) {
      return "select";
    }
    if (field === "correct" || field === "checked" || field === "approved") {
      return "toggle";
    }

    return "text";
  }

  // fetch category or ctalist for select inputs
  fetchDataForSelectInput(fieldsToFetchForSelect) {
    var authToken = this.props.location.state.token;
    for (let i = 0; i < fieldsToFetchForSelect.length; i++) {
      var fetchCall = {
        method: "GET",
        url: `http://api.parmi.io/api/${fieldsToFetchForSelect[i]}`,
        headers: {
          Authorization: "Bearer " + authToken,
          "Content-Type": "application/x-www-form-urlencoded",
        },
        json: true,
      };
      axios(fetchCall)
        .then((response) => {
          // handle success
          console.log("----response---");
          console.log(response);
          console.log(response.data["hydra:member"]);

          // need to transform words
          var field;
          var fetchedData = this.props.location.state.fetchedData;
          console.log("+++++++++++++");
          console.log(fetchedData);
          console.log("+++++++++++++");

          if (fieldsToFetchForSelect[i] === "categories") {
            field = "Category";
          }
          if (
            fieldsToFetchForSelect[i] === "cta_lists" &&
            _.has(fetchedData[0], "ctaList")
          ) {
            field = "ctaList";
          }
          if (
            fieldsToFetchForSelect[i] === "cta_lists" &&
            _.has(fetchedData[0], "ctaLists")
          ) {
            field = "ctaLists";
          }

          console.log(fieldsToFetchForSelect);

          // neet to create array with 'value' and 'label' only keys,
          // so that the select can read the correct values
          var data = response.data["hydra:member"];
          var dataFinal = [];
          data.forEach((el) => {
            console.log("===aaa===");
            console.log(el);
            dataFinal.push({ value: el.name, label: el.name });
          });

          this.setState((prevState) => ({
            dataLoaded: true,
            fetchDataForSelectInput: {
              // object that we want to update
              ...prevState.fetchDataForSelectInput, // keep all other key-value pairs
              [field]: dataFinal, // update the value of specific key in [] so it is a variable
            },
          }));
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .finally(function () {
          // always executed
        });
    }
  }

  // answer options handling
  addNewInputField() {
    var arr = this.state.answerOptions;
    var last = _.last(arr);
    arr.push(last + 1);
    console.log(arr);

    this.setState({
      answerOptions: arr,
    });
  }

  removeNewInputField(field, i) {
    var newArr = [];
    var newArr2 = [];

    var inputVals = this.state.inputValues[field];
    if (this.state.answerOptions.length > 1) {
      var arr = this.state.answerOptions;

      newArr = _.remove(arr, function (e) {
        return i === e;
      });
      newArr2 = inputVals.splice(i, 1, "");

      // console.log('i');
      // console.log(i);
      // console.log('indice');
      // console.log('ANSWER OPTIONS');
      // console.log(inputVals);
      // console.log('NUOVO ARRAY');
      // console.log(newArr2);
      // console.log('ARRAY');
      // console.log(arr);

      this.setState((prevState) => ({
        answerOptions: arr,
        inputValues: {
          // object that we want to update
          ...prevState.inputValues, // keep all other key-value pairs
          [field]: inputVals, // update the value of specific key in [] so it is a variable
        },
      }));
      setTimeout(() => {
        console.log("STATE");
        console.log(inputVals);
        console.log("this.state.inputValues");
        console.log(this.state.inputValues);
      }, 1000);
    }
  }

  //----

  // this method changes the input ui according to the select type

  handleSelectType() {
    if (this.state.inputValues["type"] !== undefined) {
      this.setState((prevState) => ({
        answerOptions: [1],
        inputValues: {
          // object that we want to update
          ...prevState.inputValues, // keep all other key-value pairs
          ["answerOption"]: [], // update the value of specific key
        },
      }));
      // resetto gli input fields delle multiple answers al cambio
      document.getElementById("multiple-answers-form").reset();

      var forbiddenInputs = this.state.forbiddenInputs;

      if (this.state.inputValues["type"]["value"] === "Parola") {
        forbiddenInputs.pop("answerOption");
        this.setState({
          isSingleAnswer: true,
          forbiddenInputs: forbiddenInputs,
        });
        console.log("PAROLAAAAAA");
      }
      if (this.state.inputValues["type"]["value"] === "Risposta Multipla") {
        forbiddenInputs.pop("answerOption");
        this.setState({
          isSingleAnswer: false,
          forbiddenInputs: forbiddenInputs,
        });
        console.log("RISPMULTIIIIII");
      }
      if (this.state.inputValues["type"]["value"] === "Foto") {
        forbiddenInputs.push("answerOption");

        this.setState({
          forbiddenInputs: forbiddenInputs,
        });
        console.log("FOTOOOOOO");
      }
    }
  }

  // this method refactor the fields for the needs of the tab
  async refactorFieldsForCreateThisElement() {
    var forbiddenInputs = ["@id", "id", "@type"];

    // to add more fields to cta lists
    if (this.props.location.state.key === "cta_lists") {
      forbiddenInputs.push("correct");
      forbiddenInputs.push("checked");

      // gestisco select per il tipo di risposta
      var answerTypes = [
        { value: "Risposta Multipla", label: "Risposta Multipla" },
        { value: "Parola", label: "Parola" },
        { value: "Foto", label: "Foto" },
      ];

      this.setState((prevState) => ({
        fetchDataForSelectInput: {
          // object that we want to update
          ...prevState.fetchDataForSelectInput, // keep all other key-value pairs
          ["type"]: answerTypes, // update the value of specific key
        },
      }));

      // hide the correct toggle for user answers form
      this.setState((prevState) => ({
        hideToggle: {
          // object that we want to update
          ...prevState.hideToggle, // keep all other key-value pairs
          ["correct"]: true, // update the value of specific key
          ["checked"]: true, // update the value of specific key
        },
      }));
      this.setState({
        // fetchedDataForEditThisElement: fetchedDataForEditThisElement,
        // fields: fields
      });
    }

    console.log("forbiddenInputs");
    console.log(forbiddenInputs);

    this.setState({
      forbiddenInputs: forbiddenInputs,
    });
  }

  //----handle image loading

  handleChange(event) {
    this.setState({
      imageFile: URL.createObjectURL(event.target.files[0]),
    });
  }

  //---

  async initCreate() {
    await this.setFetchedData();
    await this.fetchCategories();
    this.fetchDataForSelectInput(this.state.fieldsToFetchForSelect);
    await this.refactorFieldsForCreateThisElement();
  }
  componentDidMount() {
    this.initCreate();
    setTimeout(() => {
      console.log("this.state.fetchDataForSelectInput---");
      console.log(this.state.fetchDataForSelectInput);
      console.log("this.state.fieldsToFetchForSelect++++++");
      console.log(this.state.fieldsToFetchForSelect);
    }, 2000);
  }

  // RENDER
  render() {
    return (
      <div className="formElementsBox">
        {this.state.showSubmitToast ? (
          <div
            className={
              this.state.fadeToast
                ? "fade-out-splash submitToast"
                : "submitToast"
            }
          >
            {this.state.toastMessage}
          </div>
        ) : null}

        {this.state.dataLoaded ? (
          <form className="adminForm" onSubmit={this.handleSubmit}>
            <div className="formTitle">create {this.state.title}</div>
            {this.state.fields.map((field, index) => (
              <React.Fragment key={index}>
                <div className="inputFieldsContainer">
                  {!this.state.forbiddenInputs.includes(field) ? (
                    <label>{field}</label>
                  ) : null}

                  {/* input */}
                  {!this.state.forbiddenInputs.includes(field) &&
                  field !== "answerOption" &&
                  (this.setInputType(field) === "text" ||
                    this.setInputType(field) === "number" ||
                    this.setInputType(field) === "email") ? (
                    <input
                      type={this.setInputType(field)}
                      name="name"
                      value={this.setInputValue(field)}
                      onChange={(event) => this.textInputHandler(event, field)}
                      placeholder={field}
                      className="formName"
                      disabled={
                        this.state.forbiddenInputs.includes(field)
                          ? true
                          : false
                      }
                    />
                  ) : null}

                  {/* multiple input */}
                  {field === "answerOption" && !this.state.showFotoInput ? (
                    <form id="multiple-answers-form">
                      {this.state.answerOptions.map((index) => (
                        <React.Fragment key={index}>
                          <div className="inputFieldAnswer">
                            {!this.state.isSingleAnswer ? (
                              <Toggle
                                checked={this.state.correctAnswerObject[index]}
                                defaultChecked={false}
                                className="custom-classname"
                                onChange={(event) =>
                                  this.setCorrectAnswerCheckbox(event, index)
                                }
                              />
                            ) : null}
                            <input
                              type={this.setInputType(field)}
                              name="name"
                              value={this.setInputValue(field)}
                              onChange={(event) =>
                                this.multipleTextInputHandler(
                                  event,
                                  field,
                                  index
                                )
                              }
                              placeholder={field}
                              className="formName formAnswer"
                              disabled={
                                this.state.forbiddenInputs.includes(field)
                                  ? true
                                  : false
                              }
                            />
                            {index > 1 ? (
                              <FontAwesomeIcon
                                icon={faWindowClose}
                                className="removeAnswer"
                                onClick={() =>
                                  this.removeNewInputField(field, index)
                                }
                              />
                            ) : null}
                          </div>
                        </React.Fragment>
                      ))}
                      {this.state.inputValues["answerOption"] &&
                      !this.state.isSingleAnswer ? (
                        <div
                          className="addAnswer"
                          onClick={this.addNewInputField}
                        >
                          Add answer
                        </div>
                      ) : null}
                    </form>
                  ) : null}

                  {/* select */}
                  {this.setInputType(field) === "select" &&
                  this.props.location.state.key !== "categories" ? (
                    <Select
                      closeMenuOnSelect={false}
                      defaultValue={
                        field === "type"
                          ? this.state.fetchDataForSelectInput[field][0]
                          : null
                      }
                      isMulti={field === "type" ? false : true}
                      isDisabled={
                        this.props.location.state.key === "categories"
                          ? true
                          : false
                      }
                      name=""
                      options={this.state.fetchDataForSelectInput[field]}
                      className="basic-multi-select selectField"
                      classNamePrefix="select"
                      onChange={(event) =>
                        this.selectInputHandler(event, field)
                      }
                    />
                  ) : null}

                  {/* toggle */}
                  {this.setInputType(field) === "toggle" &&
                  this.state.hideToggle[field] !== true ? (
                    <Toggle
                      defaultChecked={false}
                      className="custom-classname"
                      onChange={(event) =>
                        this.toggleInputHandler(event, field)
                      }
                    />
                  ) : null}
                  {/* --==-- */}
                  {this.state.inputErrors[field] ? (
                    <div className="error">{this.state.inputErrors[field]}</div>
                  ) : (
                    <div className="error"></div>
                  )}
                </div>
              </React.Fragment>
            ))}
            {/* image input */}
            {this.props.location.state.key === "categories" ? (
              <div>
                <input type="file" onChange={this.handleChange} />
                <img src={this.state.imageFile} />
              </div>
            ) : null}
            <div className="submitBtnRow">
              <div className="submitBtn">
                <input type="submit" value="SEND" className="submitBtnInput" />
                <FontAwesomeIcon icon={faChevronRight} className="iconSubmit" />
              </div>
            </div>
          </form>
        ) : (
          <div className="spinnerBox">
            <Spinner color="" className="spinner" />
          </div>
        )}
      </div>
    );
  }
}
export default translate()(Create);
