import React, { Component } from "react";
import { translate } from "react-i18next";
import "./main.scss";
import { Link } from "react-router-dom";
import { Spinner } from "reactstrap";
import { db } from "../portfolio_single_page/portfolio_sp";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFolder } from "@fortawesome/free-solid-svg-icons";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Table from "../table/table";
import Create from "../create/create";
import Edit from "../edit/edit";

class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      usersLoaded: false,
      users: [],
      navItems: [
        {
          name: "Categories",
          url: "/categories",
          key: "categories",
          active: false
        },
        { name: "CtaList", url: "/cta_lists", key: "cta_lists", active: false },
        { name: "Users", url: "/users", key: "users", active: false },
        // {
        //   name: "CAnswers",
        //   url: "/canswers",
        //   key: "c_answers",
        //   active: false
        // },
        {
          name: "User answers",
          url: "/user_answers",
          key: "user_answers",
          active: false
        },
        {
          name: "Delivered Rewards",
          url: "/delivered_rewards",
          key: "delivered_rewards",
          active: false
        }
      ]
    };
  }

  handleActive(id) {
    var myItems = this.state.navItems;
    for (let i = 0; i < myItems.length; i++) {
      if (i === id) {
        myItems[i]["active"] = true;
      } else {
        myItems[i]["active"] = false;
      }
    }
    setTimeout(() => {
      this.setState({
        navItems: myItems
      });
    }, 200);
  }
  componentDidMount() { }

  render() {
    return (
      <div className="mainBox">
        <Router>
          <div className="navigation">
            {this.state.navItems.map((element, index) => (
              <React.Fragment key={index}>
                <Link
                  className={
                    element.active
                      ? "navigationElement activeNavigationElement"
                      : "navigationElement"
                  }
                  to={{
                    pathname: element.url,
                    state: {
                      title: element.name,
                      key: element.key
                    }
                  }}
                  onClick={() => this.handleActive(index)}
                >
                  <FontAwesomeIcon
                    icon={faFolder}
                    className={element.active ? "activeIcon" : "folderIcon"}
                  />
                  {element.name}
                </Link>
              </React.Fragment>
            ))}
          </div>
          <div className="routerContainer">
            <Route exact path="/categories" component={Table} />
            <Route exact path="/cta_lists" component={Table} />
            <Route exact path="/users" component={Table} />
            <Route exact path="/c_answers" component={Table} />
            <Route exact path="/user_answers" component={Table} />
            <Route exact path="/delivered_rewards" component={Table} />
            <Route path="/:key/create" component={Create} />
            <Route exact path="/:key/edit/:id" component={Edit} />
          </div>
        </Router>
      </div>
    );
  }
}
export default translate()(Main);
