/* eslint-disable no-loop-func */
import React, { Component } from "react";
import { translate } from "react-i18next";
import "./edit.scss";
import { Link } from "react-router-dom";
import { Spinner } from "reactstrap";
import { db } from "../portfolio_single_page/portfolio_sp";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowRight,
  faArrowLeft,
  faArrowAltCircleLeft,
  faWindowClose,
  faPlus,
  faChevronRight,
  faCross
} from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import { BrowserRouter as Router, Route } from "react-router-dom";
import TopSearch from "../topSearch/topSearch";
import {
  useTable,
  useGroupBy,
  useFilters,
  useSortBy,
  useExpanded,
  usePagination
} from "react-table";
import { test } from "../shared_methods";
import _ from 'lodash';
import Select from 'react-select';
import Toggle from 'react-toggle';
import "react-toggle/style.css";
import qs from 'querystring';
import { Base64 } from 'js-base64';




class Edit extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: this.props.location.state.title,
      precompilationValue: '',
      key: "",
      fields: [],
      fieldsWithType: [],
      forbiddenInputs: ['@id', 'id', '@type'],
      fieldsToFetchForSelect: ['cta_lists', 'categories'],
      fetchDataForSelectInput: [],
      inputValues: [],
      inputErrors: [],
      answerOptions: [1],
      toastMessage: '',
      dataLoaded: false,
      fetchedData: [],
      lastID: null,
      loadSearchBar: false,
      token: "",
      deletePopup: false,
      idElementToDelete: null,
      showSubmitToast: false,
      fadeToast: false,
      categoriesList: [],
      pagesNumber: {},
      fetchSingleFieldData: [],
      renderSelect: false,
      hideSendBtn: false,
      hideToggle: [],
      hideTrueFalseValuesToggle: [],
      isSingleAnswer: false,

      // initial values for edit
      fetchedDataForEditThisElement: [],
      fetchedDataRawForPaths: [],
      initialValues: [],
      inputChanged: [],
      initialValueSelect: [],
      inputChangedAnswerOptions: [],
      initialValuesAnswerOptions: [],
      initialValuesForReset: [],
      ctaListLoaded: false,
      userPathVariableForState: '',
      multipleCorrectAnswerIndex: 0,
      correctAnswerObject: {},
      correctIndexEdit: null,
      loadCorrectToggle: false,
      correctAnswerId: null,
    };

    this.textInputHandler = this.textInputHandler.bind(this);
    this.addNewInputField = this.addNewInputField.bind(this);
    this.removeNewInputField = this.removeNewInputField.bind(this);
    this.resetEditing = this.resetEditing.bind(this);

  }

  //form handlers=====


  // TEXT INPUT HANDLER
  textInputHandler = (event, field) => {
    // error handling
    var arrLenght = Array.from(event.target.value).length;
    var type;
    for (const key in this.state.fieldsWithType) {
      if (key === field) {
        // console.log('this.state.fieldsWithType[key]');
        type = this.state.fieldsWithType[key];
        // console.log(type);
      }
    }
    // error based on string length
    if (type === 'string') {
      if (arrLenght < 4) {
        this.setState(prevState => ({
          inputErrors: {
            // object that we want to update
            ...prevState.inputErrors, // keep all other key-value pairs
            [field]: "This field must be at least 4 characters long!" // update the value of specific key
          }
        }));
      } else {
        this.setState(prevState => ({
          inputErrors: {
            // object that we want to update
            ...prevState.inputErrors, // keep all other key-value pairs
            [field]: null // update the value of specific key
          }
        }));
      }
      if (arrLenght === 0) {
        this.setState(prevState => ({
          inputErrors: {
            // object that we want to update
            ...prevState.inputErrors, // keep all other key-value pairs
            [field]: null // update the value of specific key
          }
        }));
      }
    }
    // error based on number range
    if (type === 'number') {
      if (event.target.value < 0) {
        this.setState(prevState => ({
          inputErrors: {
            // object that we want to update
            ...prevState.inputErrors, // keep all other key-value pairs
            [field]: "Please, insert positive number" // update the value of specific key
          }
        }));
      } else {
        this.setState(prevState => ({
          inputErrors: {
            // object that we want to update
            ...prevState.inputErrors, // keep all other key-value pairs
            [field]: null // update the value of specific key
          }
        }));
      }
      if (arrLenght === 0) {
        this.setState(prevState => ({
          inputErrors: {
            // object that we want to update
            ...prevState.inputErrors, // keep all other key-value pairs
            [field]: null // update the value of specific key
          }
        }));
      }
    }
    // error based on boolean
    if (type === 'boolean') {
      // console.log('this.state.inputValues[field]--------');
      // console.log(this.state.inputValues[field]);
      // console.log(event.target.value);

      if (event.target.value !== 'true' && event.target.value !== 'false') {
        this.setState(prevState => ({
          inputErrors: {
            // object that we want to update
            ...prevState.inputErrors, // keep all other key-value pairs
            [field]: "Please, insert boolean" // update the value of specific key
          }
        }));
      } else {
        this.setState(prevState => ({
          inputErrors: {
            // object that we want to update
            ...prevState.inputErrors, // keep all other key-value pairs
            [field]: null // update the value of specific key
          }
        }));
      }
      if (arrLenght === 0) {
        this.setState(prevState => ({
          inputErrors: {
            // object that we want to update
            ...prevState.inputErrors, // keep all other key-value pairs
            [field]: null // update the value of specific key
          }
        }));
      }
    }
    // error based on string regexp for email
    if (field === 'email') {
      if (
        arrLenght < 5 ||
        !event.target.value
          .toLowerCase()
          .match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)
      ) {
        this.setState({
          emailError: "Invalid email"
        });
      } else {
        this.setState({
          emailError: null
        });
      }
      if (arrLenght === 0) {
        this.setState({
          emailError: null
        });
      }
    }

    //++++check if input has ever changed, so in the editing, if it has never been
    // modified, it shows the initial value, but if you modify it, it shows the
    // edited value

    this.setState(prevState => ({
      inputChanged: {
        // object that we want to update
        ...prevState.inputChanged, // keep all other key-value pairs
        [field]: true // update the value of specific key in [] so it is a variable
      }
    }));

    //===handling change of input field
    console.log("====================================");
    console.log('---event.target.value');
    console.log(event.target.value);
    console.log('---field');
    console.log(field);
    console.log("====================================");
    var value;

    if (this.setInputType(field) === 'number') {
      value = parseInt(event.target.value);
    } else {
      value = event.target.value;
    }
    this.setState(prevState => ({
      inputValues: {
        // object that we want to update
        ...prevState.inputValues, // keep all other key-value pairs
        [field]: value // update the value of specific key in [] so it is a variable
      }
    }));

    setTimeout(() => {
      console.log('this.state.inputValues');
      console.log(this.state.inputValues);
      console.log('this.state.inputErrors');
      console.log(this.state.inputErrors);
    }, 100);

  };

  // MULTIPLE FIELDS HANDLER

  multipleTextInputHandler = (event, field, index) => {
    // case of multiple input 
    // answerOptions
    console.log("====================================");
    console.log(event.target.value);
    console.log("====================================");
    if (this.state.inputValues[field]) {
      var values = this.state.inputValues[field];
    } else {
      values = [];
    }


    //++++check if input has ever changed, so in the editing, if it has never been
    // modified, it shows the initial value, but if you modify it, it shows the
    // edited value

    this.setState(prevState => ({
      inputChangedAnswerOptions: {
        // object that we want to update
        ...prevState.inputChangedAnswerOptions, // keep all other key-value pairs
        [index]: true // update the value of specific key in [] so it is a variable
      }
    }));
    //----

    values[index] = event.target.value;
    this.setState(prevState => ({
      inputValues: {
        // object that we want to update
        ...prevState.inputValues, // keep all other key-value pairs
        [field]: values // update the value of specific key in [] so it is a variable
      }
    }));

    setTimeout(() => {
      console.log('this.state.inputValues');
      console.log(this.state.inputValues);
      console.log('this.state.inputErrors');
      console.log(this.state.inputErrors);
    }, 100);
  }

  //TOGGLE HANDLER
  toggleInputHandler = (event, field) => {
    //===handling change of toggle field
    console.log("====================================");
    console.log(event.target.checked);
    console.log(field);
    console.log("====================================");
    var value = event.target.checked;
    this.setState(prevState => ({
      inputValues: {
        // object that we want to update
        ...prevState.inputValues, // keep all other key-value pairs
        [field]: value // update the value of specific key in [] so it is a variable
      }
    }));

    setTimeout(() => {
      console.log(this.state.inputValues);
      console.log(this.state.inputErrors);
    }, 100);
  }

  //SELECT HANDLER
  selectInputHandler = (event, field) => {
    //===handling change of toggle field
    console.log("====================================");
    console.log(event);
    console.log(field);
    console.log("====================================");
    var value = event;
    this.setState(prevState => ({
      inputValues: {
        // object that we want to update
        ...prevState.inputValues, // keep all other key-value pairs
        [field]: value // update the value of specific key in [] so it is a variable
      }
    }));

    setTimeout(() => {
      // this method changes the input ui according to the select type
      this.handleSelectType();
      console.log(this.state.inputValues);
      console.log(this.state.inputErrors);
    }, 100);
  }

  //handle submit
  handleSubmit = event => {
    // TODO:
    // method called here only to mock data bypassing the error 
    // checks
    this.finalDataJson();
    setTimeout(() => {
      this.submitToApi();
    }, 1000);

    // Calculating the lenght of the objects of values and errors
    var inputErrorsSize = 0;
    for (const key in this.state.inputErrors) {
      if (this.state.inputErrors[key] !== null) {
        inputErrorsSize += 1;
      }
    }
    var inputValuesSize = 0;
    for (const key in this.state.inputValues) {
      if (this.state.inputValues[key] !== null && this.state.inputValues[key] !== '') {
        inputValuesSize += 1;
      }
    }
    // Calculating the actual fields length, without hidden fields!
    var actualFieldsFiltered = this.state.fields.filter((field) => this.state.forbiddenInputs.includes(field) === false);

    // console.log("====================lengths================");
    // console.log(inputErrorsSize);
    // console.log(inputValuesSize);
    // console.log(actualFieldsFiltered.length);
    // console.log(actualFieldsFiltered);
    // console.log("====================================");

    if (inputErrorsSize === 0 && inputValuesSize === actualFieldsFiltered.length) {
      // TODO:
      //this is the real submit method, not the one above
      // this.finalDataJson();
      // setTimeout(() => {
      //   this.submitToApi();
      // }, 1000);
      this.setState({
        showSubmitToast: true,
        toastMessage: 'Element successfully added!',
        inputValues: []
      });
      setTimeout(() => {
        this.setState({ fadeToast: true });
      }, 1500);
      setTimeout(() => {
        this.setState({ showSubmitToast: false, fadeToast: false });
      }, 3500);
    } else {
      this.setState({
        showSubmitToast: true,
        toastMessage: 'Error',
      });
      setTimeout(() => {
        this.setState({ fadeToast: true });
      }, 1500);
      setTimeout(() => {
        this.setState({ showSubmitToast: false, fadeToast: false });
      }, 3500);
    }
    event.preventDefault();
  };


  // TODO:
  // We need to update the final data, to let them be accepted by the backend
  finalDataJson() {
    var inputData = this.state.inputValues;
    var finalData = this.state.inputValues;


    console.log('finalData');
    console.log(finalData);



    for (const key in finalData) {

      console.log('key');
      console.log(key);


      if (key === 'ctaLists') {
        // for (const val in inputData[key]) {
        //   console.log('STAMPA');
        //   console.log(inputData[key][val]['value']);
        //   finalData[key].splice(finalData[key], 1, inputData[key][val]['value']);
        // }
        for (let i = 0; i < inputData[key].length; i++) {
          console.log('STAMPA');
          console.log(inputData[key][i]['value']);
          finalData[key].splice(finalData[key][i], 1, inputData[key][i]['value']);
        }

      }



      // it removes possible empty, null or "" value in the answer option
      if (key === 'answerOption') {
        finalData[key] = inputData[key];
        _.remove(finalData[key], function (n) {
          return n === '' || n === null || n === undefined;
        });
      }

      // it switches the category name with the path: /api/categories/{id}
      if (key === 'Category') {
        var categoriesNamesInput = [];
        var categoriesInput = inputData[key];
        for (let i = 0; i < categoriesInput.length; i++) {
          console.log('cat------');
          console.log(categoriesInput[i].value);
          categoriesNamesInput.push(categoriesInput[i].value);
        }
        console.log('categoriesNamesInput');
        console.log(categoriesNamesInput);
        var listaCategorie = this.state.categoriesList
        for (let i = 0; i < listaCategorie.length; i++) {
          console.log('listaCategorie[i][]');
          console.log(listaCategorie[i]['name']);


          if (_.includes(categoriesNamesInput, listaCategorie[i]['name'])) {
            console.log('trovato');

            finalData[key].push(listaCategorie[i]['@id']);
          }
        }
        _.remove(finalData[key], function (n) {
          return typeof n !== 'string';
        });

        console.log('finalData[key].....');
        console.log(finalData[key]);

      }

    }

    // for user answers, i refactor the final array to patch
    if (this.props.location.state.key === 'user_answers') {
      delete finalData['points'];
      delete finalData['description'];
      delete finalData['question'];
      delete finalData['ctaList_name'];
      delete finalData['ctaList_answerOption'];
      delete finalData['user_name'];

      delete finalData['@id'];
      delete finalData['@type'];
      delete finalData['id'];

      if (finalData['userOption'] === undefined || finalData['userOption'] === -1) {
        delete finalData['userOption'];
      }
      if (finalData['wordUser'] === "") {
        delete finalData['wordUser'];
      }

      console.log('this.state.userPathVariableForState');
      console.log(this.state.userPathVariableForState);


      finalData['User'] = this.state.userPathVariableForState;



      console.log('datifinali');
      console.log(finalData);


    }

    if (this.props.location.state.key === 'cta_lists') {
      console.log('FINALE2');
      console.log(finalData);
      if (finalData['type']['value'] === 'Risposta Multipla') {
        finalData['type'] = 0;
      }
      if (finalData['type']['value'] === 'Parola') {
        finalData['type'] = 1;
      }
      if (finalData['type']['value'] === 'Foto') {
        finalData['type'] = 2;
      }

    }

    this.setState({

      finalData: finalData,
    });
    setTimeout(() => {
      console.log('finalData-------');
      console.log(finalData);
    }, 1000);
  }

  // fetch categories (helper method to switch the category name with path)

  async fetchCategories() {
    var authToken = this.props.location.state.token;

    var fetchCall = {
      method: "GET",
      url: `http://api.parmi.io/api/categories`,
      headers: {
        Authorization: "Bearer " + authToken,
        "Content-Type": "application/x-www-form-urlencoded"
      },
      json: true
    };
    await axios(fetchCall)
      .then(response => {
        // handle success
        console.log('----response---');
        console.log(response.data);
        console.log(response.data["hydra:member"]);
        this.setState({
          categoriesList: response.data["hydra:member"],
        });
        console.log('this.state.categoriesList----');
        console.log(this.state.categoriesList);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .finally(function () {
        // always executed

      });
  }

  // axios call to submit data [patch]
  submitToApi() {
    var authToken = this.props.location.state.token;
    // var data =
    // {
    //   "name": "test_1_mod",
    //   "description": "string",
    //   "Category": ["api/categories/25"],
    //   "points": 0,
    //   "Question": "string",
    //   "answerOption": [
    //     "string"
    //   ],
    //   "type": 0,
    //   "ctaRank": 0
    // }
    var data = this.state.finalData;

    axios.patch(`https://api.parmi.io/api/${this.props.location.state.key}/${this.props.location.state.id}`, JSON.stringify(data), {
      headers: {
        'Authorization': "Bearer " + authToken,
        "accept": "application/ld+json",
        "Content-Type": "application/merge-patch+json"
      }
    }
    ).then(response => {
      // handle success
      console.log('----response---');
      console.log(response);


      // if cta , i submit also the correct answer
      this.submitCorrectAnswerToApi((response.data['id']));

    })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .finally(function () {
        // always executed
        console.log('data+++++++');
        console.log(data);
        // console.log(this.props.location.state.id);



      });
  }


  async submitCorrectAnswerToApi(id) {
    if (this.props.location.state.key === "cta_lists") {

      var authToken = this.props.location.state.token;

      // var data =
      // {
      //   "name": "test_1",
      //   "description": "string",
      //   "Category": ["api/categories/25"],
      //   "points": 0,
      //   "Question": "string",
      //   "answerOption": [
      //     "string"
      //   ],
      //   "type": 0,
      //   "ctaRank": 0
      // }


      var data = this.state.finalData;
      console.log('iDatifinali');
      console.log(data);
      var dataForCorrectAnswer;


      if (this.state.finalData['type'] === 0) {
        dataForCorrectAnswer = {
          "TypeId": 0,
          "correctedOption": this.state.multipleCorrectAnswerIndex,
          "ctaListId": id,
        }
      }
      if (this.state.finalData['type'] === 1) {
        dataForCorrectAnswer = {
          "TypeId": 1,
          "word": this.state.finalData['answerOption'][0],
          "ctaListId": id,
        }
      }

      console.log('dataForCorrectAnswer');
      console.log(dataForCorrectAnswer);
      console.log(`the correct answer is ${this.state.finalData['answerOption'][this.state.multipleCorrectAnswerIndex]}`);




      axios.put(`https://api.parmi.io/api/c_answers/${this.state.correctAnswerId}`, JSON.stringify(dataForCorrectAnswer), {
        headers: {
          'Authorization': "Bearer " + authToken,
          "accept": "application/ld+json",
          "Content-Type": "application/ld+json"
        }
      }
      ).then(response => {
        // handle success
        console.log('----response--canswee-');
        console.log(response);
      })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .finally(function () {
          // always executed
          console.log('data+++++++correct answer');
          console.log(dataForCorrectAnswer);


        });
    }
  }


  // set correct answer checkbox fo multiple answer

  setCorrectAnswerCheckbox(event, index) {

    var multipleCorrectAnswerIndex;
    var correctAnswerObject = this.state.correctAnswerObject;

    var answerOptions = this.state.answerOptions;
    console.log('correctAnswerObject');
    console.log(correctAnswerObject);


    console.log('answerOptions');
    console.log(answerOptions);

    for (const key in correctAnswerObject) {
      if (key !== index) {
        correctAnswerObject[key] = false;
      } else {
        correctAnswerObject[key] = true;

      }

      multipleCorrectAnswerIndex = _.indexOf(answerOptions, index);
      console.log('indice');
      console.log(multipleCorrectAnswerIndex);
    }

    console.log('event.target.checked');
    console.log(event.target.checked);
    var value = event.target.checked;
    this.setState(prevState => ({
      multipleCorrectAnswerIndex: multipleCorrectAnswerIndex,
      correctAnswerObject: {
        // object that we want to update
        ...prevState.correctAnswerObject, // keep all other key-value pairs
        [index]: value // update the value of specific key in [] so it is a variable
      }
    }));

    setTimeout(() => {
      console.log('this.state.correctAnswerObject');
      console.log(this.state.correctAnswerObject);
      console.log('this.state.multipleCorrectAnswerIndex');
      console.log(this.state.multipleCorrectAnswerIndex);


    }, 500);
  }

  //=======

  // set up the key in the state of the component for future usage and then
  // call login check
  setFetchedData() {
    var data = this.props.location.state.fetchedData;
    var lastID;
    if (!_.isEmpty(data)) {
      lastID = _.last(data)['id'];
    }
    this.setState({
      fetchedData: data,
      lastID: lastID
    });

    setTimeout(() => {
      // console.log(this.state.fetchedData);
      // console.log('----LAST----');
      // console.log(this.state.lastID);
      this.setFields();
    }, 100);
  }

  // set up the fields of the selected table
  setFields() {
    var fields = [];
    var fieldsWithType = [];

    this.state.fetchedData.forEach(el => {
      for (const key in el) {
        if (!fields.includes(key) && !fieldsWithType.includes(key)) {
          // Setting the fields array and the array with the type of every field
          fields.push(key);
          fieldsWithType[key] = typeof el[key];
        }
      }
    });
    this.setState({
      fields: fields,
      fieldsWithType: fieldsWithType,
    });

    setTimeout(() => {
      console.log("this.state.fields-------");
      console.log(this.state.fields);
      console.log("this.state.fieldsWithType-------");
      console.log(this.state.fieldsWithType);
    }, 1000);
  }


  // precompile input id or set default value for all inputs
  setInputValue(field) {
    switch (field) {
      case 'id':
        return this.state.lastID + 1;
      case '@id':
        return `/ api / ${this.props.location.state.key} / ${this.state.lastID + 1}`;
      default:
        return this.state.inputValues.field
    }
  }

  //********** set type of input: here i can CONTROL THE INPUT TYPE FOR EACH FIELD ***********//
  setInputType(field) {
    if (field === 'id' || field === 'ctaRank' || field === 'points') {
      return 'number';
    }
    if (field === 'email') {
      return 'email';
    }
    if (field === 'ctaList' || field === 'ctaLists' || field === 'Category' || field === 'type') {
      return 'select'
    }
    if (field === 'correct' || field === 'checked' || field === 'approved') {
      return 'toggle'
    }
    if (field === 'imageName') {
      return 'image';
    }
    return 'text'
  }

  // fetch category or ctalist for select inputs
  async fetchDataForSelectInput(fieldsToFetchForSelect) {
    var authToken = this.props.location.state.token;
    for (let i = 0; i < fieldsToFetchForSelect.length; i++) {

      var fetchCall = {
        method: "GET",
        url: `http://api.parmi.io/api/${fieldsToFetchForSelect[i]}`,
        headers: {
          Authorization: "Bearer " + authToken,
          "Content-Type": "application/x-www-form-urlencoded"
        },
        json: true
      };
      await axios(fetchCall)
        .then(response => {
          // handle success
          // console.log('----response---');
          // console.log(response);
          // console.log(response.data["hydra:member"]);

          // need to transform words
          var field;
          var fetchedData = this.props.location.state.fetchedData;
          // console.log('+++++++++++++');
          // console.log(fetchedData);
          // console.log('+++++++++++++');


          if (fieldsToFetchForSelect[i] === 'categories') {
            field = 'Category';
          }
          if (fieldsToFetchForSelect[i] === 'cta_lists' && _.has(fetchedData[0], 'ctaList')) {
            field = 'ctaList';
          }
          if (fieldsToFetchForSelect[i] === 'cta_lists' && _.has(fetchedData[0], 'ctaLists')) {
            field = 'ctaLists';
          }

          // console.log(fieldsToFetchForSelect);

          // neet to create array with 'value' and 'label' only keys, 
          // so that the select can read the correct values
          var data = response.data["hydra:member"];
          var dataFinal = [];
          data.forEach((el) => {
            // console.log('===aaa===');
            // console.log(el);
            dataFinal.push({ 'value': el.name, 'label': el.name });
          })



          this.setState(prevState => ({
            dataLoaded: false,
            fetchDataForSelectInput: {
              // object that we want to update
              ...prevState.fetchDataForSelectInput, // keep all other key-value pairs
              [field]: dataFinal // update the value of specific key in [] so it is a variable
            }
          }));

          setTimeout(() => {
            // console.log('dataFinal for select');
            // console.log(dataFinal);

            if ('type' in this.state.fetchedDataForEditThisElement) {
              this.initialValueSelect('type');
            }

            if ('ctaLists' in this.state.fetchedDataForEditThisElement) {
              this.initialValueSelect('ctaLists');
            } else if ('ctaList' in this.state.fetchedDataForEditThisElement) {
              this.initialValueSelect('ctaList');
            }
            else if ('Category' in this.state.fetchedDataForEditThisElement) {
              this.initialValueSelect('Category');
            } else {
              console.log('errore->CONTIENE SIA CTALISTS CHE CATEGORY');
            }

          }, 1000);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .finally(function () {
          // always executed

        });
    }

  }


  // answer options handling
  addNewInputField() {
    var arr = this.state.answerOptions;
    var last = _.last(arr);
    arr.push(last + 1);
    console.log(arr);

    this.setState({
      answerOptions: arr
    });
    setTimeout(() => {
      console.log('this.state.answerOptions');
      console.log(this.state.answerOptions);

    }, 1000);
  }

  removeNewInputField(field, i) {
    var newArr = [];
    var newArr2 = [];

    var inputVals = this.state.inputValues[field];
    if (this.state.answerOptions.length > 1) {
      var arr = this.state.answerOptions;

      newArr = _.remove(arr, function (e) {
        return i === e
      })
      newArr2 = inputVals.splice(i, 1, '');

      // console.log('i');
      // console.log(i);
      // console.log('indice');
      // console.log('ANSWER OPTIONS');
      // console.log(inputVals);
      // console.log('NUOVO ARRAY');
      // console.log(newArr2);
      // console.log('ARRAY');
      // console.log(arr);

      this.setState(prevState => ({
        answerOptions: arr,
        inputValues: {
          // object that we want to update
          ...prevState.inputValues, // keep all other key-value pairs
          [field]: inputVals // update the value of specific key in [] so it is a variable
        }
      }));
      setTimeout(() => {
        console.log('STATE');
        console.log(inputVals);
        console.log('this.state.inputValues');
        console.log(this.state.inputValues);
      }, 1000);
    }

  }

  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  // =======methods for precompiled initial values for EDIT=========
  //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  // First, take this id element data, to edit
  async fetchedDataForEditThisElement(id) {
    var selectedEl;
    var fetchedData = this.props.location.state.fetchedData;
    fetchedData.forEach((v) => {
      if (v['id'] === id) {
        selectedEl = v;
      }
    })

    // console.log('+++++++selected el++++++');
    // console.log(selectedEl);
    this.setState({
      fetchedDataForEditThisElement: selectedEl,
      fetchedDataRawForPaths: selectedEl
    });
  }


  // this method changes the input ui according to the select type ON INIT
  // it is base on the numeric type (0,1,2)

  handleSelectTypeOnInitForEdit() {

    if (this.state.inputValues['type'] !== undefined) {
      console.log('DENTRO');
      console.log(this.state.inputValues['type']);



      if (this.state.inputValues['type'] === 1) {
        this.setState({
          isSingleAnswer: true,
        });
        console.log('PAROLAAAAAA');
      }
      if (this.state.inputValues['type'] === 0) {
        this.setState({
          isSingleAnswer: false,
        });
        console.log('RISPMULTIIIIII');
      }
      if (this.state.inputValues['type'] === 2) {
        this.setState({
          // isSingleAnswer: true,
        });
        console.log('FOTOOOOOO');
      }
    }

  }

  // this method changes the input ui according to the select type

  handleSelectType() {

    if (this.state.inputValues['type'] !== undefined) {


      this.setState(prevState => ({
        answerOptions: [1],
        inputValues: {
          // object that we want to update
          ...prevState.inputValues, // keep all other key-value pairs
          ['answerOption']: [],// update the value of specific key
        },
      }));

      // resetto gli input fields delle multiple answers al cambio 
      document.getElementById("multiple-answers-form-edit").reset();


      if (this.state.inputValues['type']['value'] === 'Parola') {
        this.setState({
          isSingleAnswer: true,
        });
        console.log('PAROLAAAAAA');
      }
      if (this.state.inputValues['type']['value'] === 'Risposta Multipla') {
        this.setState({
          isSingleAnswer: false,
        });
        console.log('RISPMULTIIIIII');
      }
      if (this.state.inputValues['type']['value'] === 'Foto') {
        this.setState({
          // isSingleAnswer: true,
        });
        console.log('FOTOOOOOO');
      }
    }

  }

  // this method refactor the fields for the needs of the tab

  async refactorFieldsForEditThisElement() {

    var thisEl = this.state.fetchedDataForEditThisElement;
    console.log('aaaaaaaa');
    console.log(thisEl);

    var forbiddenInputs = ['@id', 'id', '@type'];

    var userPath;

    for (const key in thisEl) {
      console.log('v------');
      console.log(thisEl);
      if (thisEl['imageName'] === null || !_.includes(thisEl, thisEl['imageName'])) {
        forbiddenInputs.push('imageName');
      }
      if (thisEl['wordUser'] === '' || !_.includes(thisEl, thisEl['wordUser'])) {
        forbiddenInputs.push('wordUser');
      }
      if (!_.includes(thisEl, thisEl['Category'])) {
        forbiddenInputs.push('Category');
      }
      if (thisEl['userOption'] === -1 || thisEl['userOption'] === undefined || !_.includes(thisEl, thisEl['userOption'])) {
        forbiddenInputs.push('userOption');
      }




      console.log('Userrr');
      console.log(thisEl['User']);

    }

    if (_.includes(thisEl, thisEl['imageName'])) {
      console.log('imagenameee');
      console.log(thisEl['imageName']);
      var imgString = thisEl['imageName'];
      var decoded = Base64.encode(imgString);
      console.log('decoded');
      console.log(decoded);


    }

    if (thisEl['User'] !== null && thisEl['User'] !== undefined) {
      var usersList;
      userPath = thisEl['User'];
      var userPathVariableForState = thisEl['User'];
      console.log('userPath');
      console.log(userPath);

      // this call awaits to call users and then set user path = to user name
      await this.fetchSingleFieldData('users').then(() => {
        var myUser;
        usersList = this.state.fetchSingleFieldData['users'];
        usersList.forEach((u) => {
          if (u['@id'] === userPath) {
            myUser = u['nickname'];
          }
        })
        var fetchedDataForEditThisElement = this.state.fetchedDataForEditThisElement;
        console.log('fetchedDataForEditThisElement-a-aa-a');
        console.log(fetchedDataForEditThisElement['User']);
        fetchedDataForEditThisElement['User'] = myUser;
        this.setState({
          fetchedDataForEditThisElement: fetchedDataForEditThisElement,
          userPathVariableForState: userPathVariableForState
        });

        setTimeout(() => {
          this.setState({
            renderSelect: true
          });
        }, 500);
      });
    }

    if (this.props.location.state.key === 'cta_lists') {
      setTimeout(() => {
        this.setState({
          renderSelect: true
        });
      }, 1000);
    }

    // to add more fields to cta lists
    if (this.props.location.state.key === 'cta_lists') {
      forbiddenInputs.push('correct');
      forbiddenInputs.push('checked');


      // gestisco select per il tipo di risposta
      var answerTypes = [
        { value: "Risposta Multipla", label: "Risposta Multipla" },
        { value: "Parola", label: "Parola" },
        { value: "Foto", label: "Foto" },
      ]

      this.setState(prevState => ({
        fetchDataForSelectInput: {
          // object that we want to update
          ...prevState.fetchDataForSelectInput, // keep all other key-value pairs
          ['type']: answerTypes,// update the value of specific key
        },
      }));



      // hide the correct toggle for user answers form
      this.setState(prevState => ({
        hideToggle: {
          // object that we want to update
          ...prevState.hideToggle, // keep all other key-value pairs
          ['correct']: true,// update the value of specific key
          ['checked']: true,// update the value of specific key

        },
        hideTrueFalseValuesToggle: {
          // object that we want to update
          ...prevState.hideTrueFalseValuesToggle, // keep all other key-value pairs
          ['correct']: true,// update the value of specific key
          ['checked']: true,// update the value of specific key

        }
      }));
      this.setState({
        // fetchedDataForEditThisElement: fetchedDataForEditThisElement,
        // fields: fields
      });
    }

    // to add more fields to user answers, related to cta lists
    if (this.props.location.state.key === 'user_answers') {
      forbiddenInputs.push('ctaList_answerOption');
      forbiddenInputs.push('ctaList_name');
      forbiddenInputs.push('user_name');


      console.log('ci sono dentro');
      var fetchedDataForEditThisElement = this.state.fetchedDataForEditThisElement;
      var fields = this.state.fields;


      await this.fetchSingleFieldData('cta_lists').then(() => {
        var myCta;
        var ctaPath = this.state.fetchedDataForEditThisElement['ctaList'];
        var ctaList = this.state.fetchSingleFieldData['cta_lists'];

        console.log('ctaListaaaaa');
        console.log(ctaList);


        ctaList.forEach((u) => {
          if (u['@id'] === ctaPath) {
            myCta = u;
            console.log('myCta');
            console.log(myCta);

          }
        })


        var userMultipleAnswerIndex = fetchedDataForEditThisElement['userOption']
        console.log('userMultipleAnswerIndex');
        console.log(userMultipleAnswerIndex);


        fetchedDataForEditThisElement['points'] = myCta.points;
        fetchedDataForEditThisElement['question'] = myCta.Question;
        fetchedDataForEditThisElement['description'] = myCta.description;
        if (userMultipleAnswerIndex !== -1 || !_.includes(fetchedDataForEditThisElement, userMultipleAnswerIndex)) {

          fetchedDataForEditThisElement['userOption'] = myCta.answerOption[userMultipleAnswerIndex];
        }


        fields.push('points');
        fields.push('question');
        fields.push('description');

        // hide the correct toggle for user answers form

        this.setState(prevState => ({
          hideToggle: {
            // object that we want to update
            ...prevState.hideToggle, // keep all other key-value pairs
            ['correct']: true // update the value of specific key
          }
        }));
        this.setState({
          // hide the send button for user answers
          hideSendBtn: true,
          fetchedDataForEditThisElement: fetchedDataForEditThisElement,
          fields: fields
        });
      });


    }

    console.log('forbiddenInputs');
    console.log(forbiddenInputs);


    this.setState({
      forbiddenInputs: forbiddenInputs
    });
  }


  //****************************************************************** */
  //======================init GENERAL fetch data process for given key 
  // (key is the api endpoint, for example 'users', 'cta_lists'..ecc)

  // fetch data from api
  // TODO: metti solo await uno dopo l altro senza i then()
  async fetchSingleFieldData(key) {
    await this.findNumberOfPages(key).then(() =>
      this.fetchDataCycle(key)
    )

    // for now it not need a reorder function such as in the table
    //   .then(() =>
    //   this.reorderFetchedData(key)
    // );
  }

  // find number of pages
  async findNumberOfPages(key) {
    var authToken = this.props.location.state.token;
    var fetchCall = {
      method: "GET",
      url: `http://api.parmi.io/api/${key}`,
      headers: {
        Authorization: "Bearer " + authToken,
        "accept": "application/ld+json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      json: true
    };
    await axios(fetchCall)
      .then(response => {
        // handle success
        console.log('IL RESPONSO 1');
        console.log(response);
        console.log(response.data);
        if (response.data['hydra:view'] !== undefined) {
          console.log('hydra:view-----');
          console.log(response.data['hydra:view']['hydra:last']);
          var string = response.data['hydra:view']['hydra:last'];
          // string.substring

          var str = string;
          var matches = str.match(/(\d+)/);

          if (matches) {
            console.log('matches');
            console.log(matches[0]);
          }


          this.setState(prevState => ({
            pagesNumber: {
              // object that we want to update
              ...prevState.pagesNumber, // keep all other key-value pairs
              [key]: parseInt(matches[0]) // update the value of specific key
            }
          }));
          setTimeout(() => {
            console.log('this.state.pagesNumber');
            console.log(this.state.pagesNumber);

          }, 500);

        } else {
          this.setState(prevState => ({
            pagesNumber: {
              // object that we want to update
              ...prevState.pagesNumber, // keep all other key-value pairs
              [key]: 1 // update the value of specific key
            }
          }));
          setTimeout(() => {
            console.log('this.state.pagesNumber');
            console.log(this.state.pagesNumber);

          }, 500);
        }


      })
      .catch(function (error) {
        // handle error
        console.log(error);
      })
      .finally(function () {
        // always executed

      });
  }

  // cycle through the pages (api platform has a fucking pagination)
  async fetchDataCycle(key) {
    // it solves a bug about canswers fetching pagination in cta_lists

    var cAnswers = [];

    console.log('SONO QUI');

    for (let i = 1; i <= this.state.pagesNumber[key]; i++) {
      console.log('SONO DENTRO AL CICLO');

      var authToken = this.props.location.state.token;

      var fetchCall = {
        method: "GET",
        url: `http://api.parmi.io/api/${key}?page=${i}`,
        headers: {
          Authorization: "Bearer " + authToken,
          "accept": "application/ld+json",
          "Content-Type": "application/x-www-form-urlencoded"
        },
        json: true
      };
      await axios(fetchCall)
        .then(response => {
          // handle success
          console.log('IL RESPONSO 2');

          console.log(response);
          console.log(response.data);
          console.log('hydra:member-----');
          console.log(response.data['hydra:member']);

          var fetchedData = this.state.fetchSingleFieldData;
          if (response.data["hydra:member"].length !== 0) {

            fetchedData[key] = response.data["hydra:member"]
            cAnswers.push(response.data["hydra:member"])

          }



          this.setState({
            loadSearchBar: true,
            fetchSingleFieldData: fetchedData
          });

          setTimeout(() => {
            // this.setFields();
            // console.log('this.props.location.state.key');
            // console.log(this.props.location.state.key);
            console.log('this.state.fetchSingleFieldData--**');
            console.log(this.state.fetchSingleFieldData);
          }, 50);


        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .finally(function () {
          // always executed

        });

    }
    if (this.props.location.state.key === 'cta_lists') {
      // it solves a bug about canswers fetching pagination in cta_lists edit
      this.state.fetchSingleFieldData['c_answers'] = cAnswers;
    }
  }

  reorderFetchedData(key) {
    var fetchedData = this.state.fetchSingleFieldData;
    var newFetchedData = [];
    for (let i = 0; i < fetchedData.length; i++) {
      console.log('fetchedData[i]');
      console.log(fetchedData[i]);
      for (let j = 0; j < fetchedData[i].length; j++) {
        console.log('fetchedData[i][j]');
        console.log(fetchedData[i][j]['@id']);

        if (fetchedData[i] !== []) {
          newFetchedData.push(fetchedData[i][j]);
          console.log('newFetchedDataInsideFor');
          console.log(newFetchedData);
        }
      }
    }
    setTimeout(() => {

      this.setState(prevState => ({
        loadSearchBar: true,
        fetchSingleFieldData: {
          // object that we want to update
          ...prevState.fetchSingleFieldData, // keep all other key-value pairs
          [key]: newFetchedData // update the value of specific key
        }
      }));
    }, 100);

    setTimeout(() => {

      console.log('nuoviUtenti');
      console.log(this.state.fetchSingleFieldData);


    }, 2000);

  }

  //======================finish fetch data process


  // Then, the initial values are set equal to the input values
  setInitialValues() {
    var initialValues = [];
    var fields = this.state.fields;
    // console.log('fields');
    // console.log(fields);

    var myEl = this.state.fetchedDataForEditThisElement;
    // console.log('myEl------');
    // console.log(myEl);

    // console.log('STAMPO VALORI');
    for (const k in fields) {
      // console.log('k');
      // console.log(k);
      for (const j in myEl) {
        // console.log('j');
        // console.log(j);
        if (fields[k] === j) {
          initialValues[fields[k]] = myEl[j];
        }
      }
    }
    console.log('initialValues-------');
    console.log(initialValues);
    this.setState({
      dataLoaded: true,
      initialValues: initialValues,
      initialValuesForReset: initialValues,
      inputValues: initialValues
    });

  }

  // multiple answer options input
  setInitialAnswerOptions() {
    console.log('this.state.initialValues for answer options');
    console.log(this.state.initialValues);
    var initVals = this.state.initialValues;
    var answerOption;
    for (const key in initVals) {
      if (key === 'answerOption') {
        answerOption = initVals[key];
      }
    }
    if (_.hasIn(initVals, 'answerOption')) {
      console.log('answerOption');
      console.log(answerOption);

      var answerOptionIndexes = [];
      if (answerOption.length === 1) {
        answerOptionIndexes.push(0);

      }
      else {

        for (let i = 0; i < answerOption.length; i++) {
          if (i !== null && i !== undefined && i !== '') {

            answerOptionIndexes.push(i);
          }
        }
      }
      console.log('answerOptionIndexes');
      console.log(answerOptionIndexes);




      this.setState({
        initialValuesAnswerOptions: answerOption,
        answerOptions: answerOptionIndexes
      });
    }


  }

  // select-----
  async initialValueSelect(field) {
    var myEl = this.state.fetchedDataForEditThisElement;
    console.log('myEl------');
    console.log(myEl);


    var mySelectFieldsApiPaths = myEl[field];
    // console.log('mySelectFieldsApiPaths');
    // console.log(mySelectFieldsApiPaths);
    var dataFinal = [];

    var url;

    var authToken = this.props.location.state.token;
    // initial value for cta type select 
    if (field === 'type') {

      var initialFieldForSelectType;
      if (myEl.type === 0) {
        initialFieldForSelectType = [{ value: "Risposta Multipla", label: "Risposta Multipla" }];
      }
      if (myEl.type === 1) {
        initialFieldForSelectType = [{ value: "Parola", label: "Parola" }];
      }
      if (myEl.type === 2) {
        initialFieldForSelectType = [{ value: "Foto", label: "Foto" }];
      }

      this.setState(prevState => ({
        initialValueSelect: {
          // object that we want to update
          ...prevState.initialValueSelect, // keep all other key-value pairs
          [field]: initialFieldForSelectType// update the value of specific key
        }
      }));
    }
    setTimeout(() => {
      console.log('pppppppp');
      console.log(this.state.initialValueSelect);

    }, 1000);

    for (let i = 0; i < mySelectFieldsApiPaths.length; i++) {

      if (field === 'ctaLists') {
        url = `http://api.parmi.io${mySelectFieldsApiPaths[i]}`

      } else if (field === 'Category') {
        url = `http://api.parmi.io${mySelectFieldsApiPaths[i]['@id']}`
      }
      var fetchCall = {
        method: "GET",
        url: url,
        headers: {
          Authorization: "Bearer " + authToken,
          "Content-Type": "application/x-www-form-urlencoded"
        },
        json: true
      };
      await axios(fetchCall)
        .then(response => {
          // handle success
          // console.log('----response SELECT initial val---');
          // console.log(response);
          // console.log(response.data["name"]);

          // neet to create array with 'value' and 'label' only keys, 
          // so that the select can read the correct values
          var data = response.data["name"];
          dataFinal.push({ 'value': data, 'label': data });
          // At the end, the state is set 
          // this.setState({
          //   initialValueSelect: dataFinal
          // });
          this.setState(prevState => ({
            initialValueSelect: {
              // object that we want to update
              ...prevState.initialValueSelect, // keep all other key-value pairs
              [field]: dataFinal // update the value of specific key
            }
          }));
          // for security, the loading is kept a bit longer
          setTimeout(() => {
            this.setState({
              ctaListLoaded: true
            });

          }, 2000);
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .finally(function () {
          // always executed
        });
    }


    // if we have only one ctaList (for UserAnswer) we can not do a cycle
    if (field === 'ctaList') {
      url = `http://api.parmi.io${mySelectFieldsApiPaths}`
      console.log('mySelectFieldsApiPaths[i]++++++++++');
      console.log(mySelectFieldsApiPaths);
      var fetchCall_2 = {
        method: "GET",
        url: url,
        headers: {
          Authorization: "Bearer " + authToken,
          "Content-Type": "application/x-www-form-urlencoded"
        },
        json: true
      };
      await axios(fetchCall_2)
        .then(response => {
          // handle success
          console.log('----response SELECT initial val---');
          console.log(response);
          console.log(response.data["name"]);

          // neet to create array with 'value' and 'label' only keys, 
          // so that the select can read the correct values
          var data = response.data["name"];
          dataFinal.push({ 'value': data, 'label': data });
          // At the end, the state is set 
          // this.setState({
          //   initialValueSelect: dataFinal
          // });
          this.setState(prevState => ({
            initialValueSelect: {
              // object that we want to update
              ...prevState.initialValueSelect, // keep all other key-value pairs
              [field]: dataFinal // update the value of specific key
            }
          }));
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .finally(function () {
          // always executed

        });
    }



    setTimeout(() => {
      console.log('data array for select default values');
      // console.log(dataFinal);
      console.log(this.state.initialValueSelect);
    }, 1000);

  }

  // set initial values of multiple answer correct answer
  async setCorrectMultipleAnswerEdit() {

    if (this.props.location.state.key === "cta_lists") {


      await this.fetchSingleFieldData('c_answers').then(() => {
        var cAnswersList = this.state.fetchSingleFieldData['c_answers'];
        var correctIndex;
        console.log('c_answers');
        console.log(cAnswersList);
        cAnswersList.forEach((cAnswersListPage) => {
          cAnswersListPage.forEach((ca) => {
            if (ca['ctaListId'] === this.props.location.state.id) {
              console.log('correct answer for this cta');
              console.log(ca);
              correctIndex = ca['correctedOption'];
              this.setState({
                correctAnswerId: ca['id'],
                correctIndexEdit: correctIndex,
                loadCorrectToggle: true,
              });
              this.setState(prevState => ({
                correctAnswerObject: {
                  // object that we want to update
                  ...prevState.correctAnswerObject, // keep all other key-value pairs
                  [correctIndex]: true // update the value of specific key
                }
              }));
            }
          })
        })

        console.log('this.state.correctIndexEdit');
        console.log(this.state.correctIndexEdit);


      })
    }






    // var authToken = this.props.location.state.token;

    // var fetchCall = {
    //   method: "GET",
    //   url: `http://api.parmi.io/api/c_answers`,
    //   headers: {
    //     Authorization: "Bearer " + authToken,
    //     "Content-Type": "application/x-www-form-urlencoded"
    //   },
    //   json: true
    // };
    // await axios(fetchCall)
    //   .then(response => {
    //     // handle success
    //     var cAnswersList = response.data['hydra:member'];
    //     var correctIndex;
    //     console.log('c_answers');
    //     console.log(cAnswersList);
    //     cAnswersList.forEach((ca) => {
    //       if (ca['ctaListId'] === this.props.location.state.id) {
    //         console.log('correct answer for this cta');
    //         console.log(ca);
    //         correctIndex = ca['correctedOption'];
    //       }
    //     })

    //   })
    //   .catch(function (error) {
    //     // handle error
    //     console.log(error);
    //   })
    //   .finally(function () {
    //     // always executed

    //   });
  }


  // BUTTONS-------

  // reset editing btn
  resetEditing() {
    var initialValuesForReset = this.state.initialValuesForReset;
    console.log('reset editing');
    this.setState({
      initialValues: initialValuesForReset,
      inputValues: initialValuesForReset,
      inputChanged: [],
    });


    setTimeout(() => {
      console.log('this.state.initialValues RESETTATI---+++---');
      console.log(this.state.initialValues);
      console.log(this.state.inputValues);
    }, 1000);

  }


  componentDidMount() {
    this.setFetchedData();
    this.fetchCategories();
    this.fetchDataForSelectInput(this.state.fieldsToFetchForSelect);
    this.fetchedDataForEditThisElement(this.props.location.state.id)
      .then(() => this.refactorFieldsForEditThisElement());


    setTimeout(() => {
      this.setInitialValues();
      this.setInitialAnswerOptions();
      this.handleSelectTypeOnInitForEdit();
      this.setCorrectMultipleAnswerEdit();

      // this.fetchSingleFieldData('cta_lists');
      // console.log('this.state.fieldsToFetchForSelect');
      // console.log(this.state.fieldsToFetchForSelect);
      // console.log('this.state.fetchDataForSelectInput');
      // console.log(this.state.fetchDataForSelectInput);
      console.log('++++++this.props.location.state.fetchedData+++++++');
      console.log(this.props.location.state.fetchedData);

      console.log('this.state.fetchedDataForEditThisElement---');
      console.log(this.state.fetchedDataForEditThisElement);

      console.log('this.state.mySelectFieldsApiPaths---');
      console.log(this.state.mySelectFieldsApiPaths);



      // console.log('this.props.location.state.fetchedDataForElement');
      // console.log(this.props.location.state.fetchedDataForElement);
    }, 3000);

    setTimeout(() => {

      console.log('this.state.initialValueSelect------');
      console.log(this.state.initialValueSelect);
    }, 4000);
  }

  // RENDER
  render() {
    return (
      <div className="formElementsBox">
        {this.state.showSubmitToast ? (
          <div className={(this.state.fadeToast) ? "fade-out-splash submitToast" : "submitToast"}>{this.state.toastMessage}</div>
        ) : null}

        {this.state.dataLoaded ? (
          <form className="adminForm" onSubmit={this.handleSubmit}>
            <div className="formButtonsHeader">
              <Link
                className="backBtnBox"
                to={{
                  pathname: `/${this.props.location.state.key}`,
                  state: {
                    title: this.props.location.state.title,
                    token: this.props.location.state.token,
                    key: this.props.location.state.key,
                    fetchedData: this.props.location.state.fetchedData,
                  }
                }}
              >
                Back to table
                  <FontAwesomeIcon icon={faArrowLeft} className="backBtnIco" />
              </Link>
              <div className="backBtnBox" onClick={this.resetEditing}>
                Reset editing
                <FontAwesomeIcon icon={faCross} className="backBtnIco" />
              </div>
            </div>
            <div className="formTitle">edit {this.state.title} with id: {this.props.location.state.id}</div>
            {this.state.fields.map((field, index) => (
              <React.Fragment key={index}>
                <div className="inputFieldsContainer">
                  {(!this.state.forbiddenInputs.includes(field)) ? <label>{field}</label> : null}

                  {/* input */}
                  {(!this.state.forbiddenInputs.includes(field) && field !== 'answerOption' && (this.setInputType(field) === 'text' || this.setInputType(field) === 'number' || this.setInputType(field) === 'email')) ? <input
                    type={this.setInputType(field)}
                    name="name"
                    value={(this.state.inputChanged[field]) ? this.setInputValue(field) : this.state.initialValues[field]}
                    onChange={
                      event => this.textInputHandler(event, field)
                    }
                    placeholder={field}
                    className="formName"
                    disabled={this.state.forbiddenInputs.includes(field) ? true : false}
                  /> : null}

                  {/* multiple input */}
                  {(field === 'answerOption') ?
                    <form id="multiple-answers-form-edit">
                      {this.state.answerOptions.map((index) => (
                        <React.Fragment key={index}>
                          <div className="inputFieldAnswer">
                            {(!this.state.isSingleAnswer && this.state.loadCorrectToggle === true) ? <Toggle
                              checked={this.state.correctAnswerObject[index]}
                              defaultChecked={(index === this.state.correctIndexEdit) ? true : false}
                              className='custom-classname'
                              onChange={event => this.setCorrectAnswerCheckbox(event, index)} /> : null}

                            <input
                              type={this.setInputType(field)}
                              name="name"
                              // value={this.setInputValue(field)}
                              value={(this.state.inputChangedAnswerOptions[index]) ? this.setInputValue(field) : this.state.initialValuesAnswerOptions[index]}
                              onChange={
                                event => this.multipleTextInputHandler(event, field, index)
                              }
                              placeholder={field}
                              className="formName formAnswer"
                              disabled={this.state.forbiddenInputs.includes(field) ? true : false}
                            />
                            {(index >= 1) ? <FontAwesomeIcon icon={faWindowClose} className="removeAnswer" onClick={() => this.removeNewInputField(field, index)} /> : null}
                          </div>

                        </React.Fragment>))}
                      {(this.state.inputValues['answerOption'] && !this.state.isSingleAnswer) ? <div className="addAnswer" onClick={this.addNewInputField}>Add answer</div> : null}

                    </form>
                    : null}

                  {/* select */}
                  {(this.setInputType(field) === 'select' && this.props.location.state.key !== 'categories' && !this.state.forbiddenInputs.includes(field) && this.state.renderSelect === true) ?
                    <Select closeMenuOnSelect={false}
                      defaultValue={this.state.initialValueSelect[field]}
                      isMulti={field === 'type' ? false : true}
                      isDisabled={field === 'type' ? true : false}
                      name=""
                      options={this.state.fetchDataForSelectInput[field]}
                      className={field === 'type' ? "basic-multi-select selectField selectFieldDisabled" : "basic-multi-select selectField"}
                      classNamePrefix="select"
                      onChange={event => this.selectInputHandler(event, field)}
                    />
                    : null}

                  {/* cta lists for category form (is only a list, and only for edit, not for create) */}
                  {(this.setInputType(field) === 'select' && this.props.location.state.key === 'categories' && this.state.ctaListLoaded) ?
                    <div className="ctaListBox">
                      {this.state.initialValueSelect[field].map((ctaListEl, index) => (
                        <React.Fragment key={index}>
                          <div className='ctaListElement'>
                            {ctaListEl.value}
                          </div>
                        </React.Fragment>
                      ))}
                    </div> : null}

                  {/* image reader */}
                  {(this.setInputType(field) === 'image' && 'imageName' in this.state.initialValues) ? <div><img src={`https://api.parmi.io/${this.state.initialValues[field]}`}></img>{this.state.initialValues[field]}</div> : null}

                  {/* toggle */}
                  {(this.setInputType(field) === 'toggle' && this.state.hideToggle[field] !== true) ?
                    <Toggle
                      defaultChecked={this.state.initialValues[field]}
                      className='custom-classname'
                      onChange={event => this.toggleInputHandler(event, field)} />
                    : null}
                  {this.setInputType(field) === 'toggle' && this.state.hideTrueFalseValuesToggle[field] !== true ? <div className="correctToggleLabel">{(this.state.initialValues[field] !== undefined) ? this.state.initialValues[field].toString().toUpperCase() : null}</div> : null}
                  {/* --==-- */}
                  {this.state.inputErrors[field] ? (
                    <div className="error">
                      {this.state.inputErrors[field]}
                    </div>
                  ) : (
                      <div className="error"></div>
                    )}
                </div>
              </React.Fragment>
            ))}
            <div className="submitBtnRow">
              {!this.state.hideSendBtn ? <div className="submitBtn">
                <input type="submit" value="SEND" className="submitBtnInput" />
                <FontAwesomeIcon icon={faChevronRight} className="iconSubmit" />
              </div> : null}
            </div>
          </form>
        ) : (
            <div className="spinnerBox">
              <Spinner color="" className="spinner" />
            </div>
          )
        }
      </div>
    );
  }
}
export default translate()(Edit);
